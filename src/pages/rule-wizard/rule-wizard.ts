import { Component, ViewChild, ElementRef } from '@angular/core';
import { NavController, NavParams, Events } from 'ionic-angular';
import { AuthProvider } from '../../providers/auth/auth';
import { DataProvider } from '../../providers/data/data';
import { SearchModel } from '../../providers/model/searchModel';
import { Step3TestLevelComponent } from "../../components/step3-test-level/step3-test-level";
import { TypeSort } from '../../providers/model/typeSort';

@Component({
  selector: 'page-rule-wizard',
  templateUrl: 'rule-wizard.html',
})
export class RuleWizardPage {

  searchModel: SearchModel;
  currentStep: number;
  testCodeList: Array<any>;
  public resultsAllData: any;
  public results: Array<any>;
  public selections: Array<string>;
  public detailSelection: any;
  public cart: Array<any>;
  public user: any;
  public username: string;
  public pager: any;
  public isLoading: boolean;
  
  public typeSort: string;
  public fieldSort: string;
  public arrSort:any;

  constructor(public navCtrl: NavController, public navParams: NavParams, private data: DataProvider, private auth: AuthProvider) {
    this.isLoading = false;
    this.currentStep = 1;
    this.searchModel = new SearchModel();
    this.testCodeList = new Array<any>();
    this.results = [];
    this.resultsAllData = {
      content: [],
      totalItems: 0
    };;
    this.detailSelection = null;
    this.selections = [];
    this.searchModel = new SearchModel();
    this.cart = [];
    this.user = this.navParams.get('user');
    if (this.auth._user) {
      this.user = this.auth._user;
      this.username = this.user.FullName;

    }
    this.arrSort = {};
    this.typeSort = TypeSort.none;
    this.fieldSort = '';
  }

  ionViewDidLoad() {
  }
  ionViewWillEnter() {
    if (this.pager) {
      this.search(this.pager.currentPage, this.fieldSort, this.typeSort);
    }
  }

  public selectStep(step) {

    let tabCurent: HTMLElement = document.querySelector(".nav-wizard .active") as HTMLElement;
    if (tabCurent != null) {
      tabCurent.classList.toggle("active");
    }

    let selector = "#tab" + step;
    let tabSelect: HTMLElement = document.querySelector(selector) as HTMLElement;

    if (tabCurent != null) {
      tabSelect.classList.toggle("active");
    }

    let contentCurent: HTMLElement = document.querySelector(".contentTab:not(.collapse)") as HTMLElement;
    if (contentCurent != null) {
      contentCurent.classList.toggle("collapse");
    }

    selector = '#contentTab' + step;
    let currentSelect: HTMLElement = document.querySelector(selector) as HTMLElement;
    if (currentSelect != null) {
      currentSelect.classList.toggle("collapse");
    }
    this.currentStep = step;
    // console.log(step);
    // if (step == 4) {
    //   this.search();

    // }
  }

  public search(page = 1, fieldSort = '', typeSort = TypeSort.none) {
    //reset State
    var tempObj = {fieldSort: fieldSort, typeSort: typeSort};
    this.arrSort = tempObj;
    let searchResultTable: HTMLElement = document.querySelector(".table-result") as HTMLElement;
    if (searchResultTable != null) {
      searchResultTable.removeAttribute("col-8");
      searchResultTable.setAttribute("col-12", "");
    }
    let detailItem: HTMLElement = document.querySelector(".detail") as HTMLElement;

    if (detailItem != null) {
      detailItem.classList.add("collapse");
    }

    let data = this.data.getDataSearch(this.searchModel);
    // this.resultsAllData = {
    //   content: [],
    //   totalItems: 0
    // };
    this.pager = this.data.getPager(this.resultsAllData.totalItems);
    this.selectStep(4);
    //api call page index from 0
    this.isLoading = true;
    this.data.search(data, page, fieldSort, typeSort)
      .subscribe(res => {
        this.results = res["content"];
        this.resultsAllData = {
          content: res["content"],
          totalItems: res["totalElements"]
        };
        this.pager = this.data.getPager(this.resultsAllData.totalItems, page);

        this.selectStep(4);
        this.isLoading = false;
      }, err => {
        console.log(err);
        this.isLoading = false;

      });
  }


  backStep() {
    this.selectStep(this.currentStep - 1);
  }

  nextStep() {
    if (this.currentStep == 3) {
      this.search(1);
    } else {
      this.selectStep(this.currentStep + 1);
    }
  }
  @ViewChild(Step3TestLevelComponent) step3: Step3TestLevelComponent;

  clear() {
    this.step3.clearSelection();
    console.log(this.testCodeList);
    this.searchModel = new SearchModel();
    this.results = [];


  }

  updateCart(event) {
    for (let i = 0; i < event.length; i++) {
      let newitem = event[i];
      if (!this.checkExists(newitem)) {
        this.cart.push(newitem);
      }
    }
    this.selectStep(1);
    this.clear();

  }
  checkExists(item) {
    for (let j = 0; j < this.cart.length; j++) {
      if (this.cart[j].ruleName == item.ruleName) {
        return true;
      }
    }
    return false;
  }

  showDetailCart() {
    console.log(this.cart);
    let panelDetailCart: HTMLElement = document.querySelector("#btnPanelDetailCart") as HTMLElement;
    panelDetailCart.click();
  }
  quitSearh() {
    // this.router.navigate(["/dashboard"]);
  }
  changePage(event) {
    this.fieldSort = event.fieldSort;
    this.typeSort = event.typeSort;
    this.search(event.pageIndex, event.fieldSort, event.typeSort);
    // this.resultsAllData.totalItems += 10;
    // this.pager = this.data.getPager(this.resultsAllData.totalItems, event.pageIndex);
  }

}
