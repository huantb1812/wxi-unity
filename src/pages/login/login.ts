import { Component, EventEmitter, Output } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';

import { AlertController, Events, LoadingController, NavController, NavParams, ViewController } from 'ionic-angular';
import { AuthProvider } from '../../providers/auth/auth';
import { Storage } from '@ionic/storage';


/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {
  @Output() updateUser: EventEmitter<any>;
  submitAttempt: boolean = false;
  loginForm: any;
  copyright: string;
  isElectron: boolean = false;
  appVersion: string;


  constructor(private alertCtrl: AlertController, private auth: AuthProvider,
    private fb: FormBuilder, private loadCtrl: LoadingController,
    private events: Events, private navParams: NavParams,
    private navCtrl: NavController,
    private storage: Storage,
    public viewCtrl: ViewController) {
    this.loginForm = this.fb.group({
      username: [null, Validators.compose([Validators.maxLength(512), Validators.required])],
      password: [null, Validators.compose([Validators.maxLength(512), Validators.required])]
    });
    this.updateUser = new EventEmitter();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');

    let u = this.navParams.get('user');
    if (u) {
      this.loginForm.setValue({
        username: u.email || null,
        password: (u.login && u.login.password) ? u.login.password : null
      });
    }



  }

  ionViewDidEnter() {
    this.copyright = '© ' + new Date().getFullYear() + ' Sysmex America, Inc.';

  }
  closeModal() {
    this.viewCtrl.dismiss(this.loginForm);
  }
  forgot() {
    console.info('forgot password');
  }

  login(event: MouseEvent = null) {

    if (!this.loginForm.valid) {
      console.warn('invalid', this.loginForm.valid, this.loginForm);
      let alert = this.alertCtrl.create({
        mode: 'ios',
        title: 'Missing Credentials',
        message: 'Please supply a username and password.',
        buttons: [{ text: 'OK' }]
      });
      alert.present();
      return;
    }
    console.log('login', event);
    let loader = this.loadCtrl.create();
    loader.present();

    this.auth.login(this.loginForm.value, this.navParams.get('user')).subscribe((result) => {
      this.updateUser.emit(result);
      loader.dismiss().then(() => {
        this.navCtrl.pop().then(() => {
          if (event && event.shiftKey) {
            console.log('shift!');
            this.events.publish('DEBUG_MODE_CHANGED', true);
          }
          this.loginForm.reset();
        })
      });
    }, (reason) => {
      console.warn('login error', reason);
      loader.dismiss().then(() => {
        let alert = this.alertCtrl.create({
          mode: 'ios',
          title: 'Login Failed',
          message: 'Please verify your credentials and try again.',
          buttons: [{ text: 'OK' }]
        });
        alert.present();
      });
    })
  }
}
