import { Component, HostListener, ViewChild, ElementRef } from '@angular/core';
import { NavController, NavParams, LoadingController } from 'ionic-angular';
import { SearchModel } from '../../providers/model/searchModel';
import { DataProvider } from '../../providers/data/data';
import { TypeSort } from '../../providers/model/typeSort';
export enum KEY_CODE {
  RIGHT_ARROW = 39,
  LEFT_ARROW = 37,
  UP_ARROW = 38,
  DOWN_ARROW = 40,
  ENTER = 13
}
@Component({
  selector: 'page-result-validation',
  templateUrl: 'result-validation.html',
})


export class ResultValidationPage {
  searchModel: SearchModel;
  public sampleIds: any;
  public patientDemoGraphics: any;
  public listOrderResultsInstrusment: any;
  public listOrderResultsManual: any;
  public listOpAlert: any;
  public modelSearch: any;
  public totalSampleId: number;

  public typeSort: string;
  public fieldSort: string;
  showOPAlert: boolean = true;
  public conditionSearch: any;
  public objArrow: any;
  public isLoading: boolean = false;
  @ViewChild('focusTable') focusTable: ElementRef;
  constructor(public navCtrl: NavController, public navParams: NavParams, private data: DataProvider, private lc: LoadingController ) {

    // let totalSampleId = res.totalElements;

    this.searchModel = new SearchModel();
    this.sampleIds = [];
    this.patientDemoGraphics = {};
    this.listOrderResultsInstrusment = [];
    this.listOrderResultsManual = [];
    this.listOpAlert = [];
    this.typeSort = TypeSort.none;
    this.fieldSort = '';
    this.conditionSearch = {};
    
  }
  // @HostListener('scroll', ['$event'])
  // onScroll(event) {
  //   event.preventDefault();
  // }
  //preventDefault();

  @HostListener('document:keydown', ['$event']) onKeydownHandler(event: KeyboardEvent) {
    if (event.keyCode === KEY_CODE.UP_ARROW) {
        // ...
        event.preventDefault();
    }
    if (event.keyCode === KEY_CODE.DOWN_ARROW) {
      event.preventDefault();
    }
}
  @HostListener('window:keyup', ['$event'])
  handleKeyboardEvent(event: KeyboardEvent) {

    // if(event.key  ===  'ArrowDown'  ||  event.key  ===  'ArrowLeft'){

    // }
    // else if(event.key  ===  'ArrowUp' ||  event.key  ===  'ArrowRight'){

    // }

    // if(event.key  ===  'Enter'){

    // }
    // if(event.keyCode === KEY_CODE.ENTER){
    //   console.log('enter');
    //   if(this.objArrow){
    //     this.getDataByArrowitem(this.objArrow);
    //   }
    //   this.objArrow =null;
    // }
    if (event.keyCode === KEY_CODE.UP_ARROW) {
      if(!this.isLoading){
        this.isLoading = true;
        console.log('up');
       
        var tempObj;
        for(var i = 0; i < this.sampleIds.length; i++){
            var currentItem = this.sampleIds[i];
            if(currentItem.active == true && i != 0 ){
              currentItem.active = false;
              var tempObj = this.sampleIds[i-1];
              tempObj.active = true;            
              this.objArrow = tempObj;
              this.focusTable.nativeElement.scrollTop = this.focusTable.nativeElement.scrollTop - 73;           
              break;
            }
        }
        if(tempObj){        
          this.getDataByArrowitem(tempObj);
        }else{
          this.isLoading = false;
        }
      }
      
    }

    if (event.keyCode === KEY_CODE.DOWN_ARROW) {
      if(!this.isLoading){
        this.isLoading = true;
        console.log('down');
        
        var tempObj;
        for(var i = 0; i < this.sampleIds.length; i++){
            var currentItem = this.sampleIds[i];
            if(currentItem.active == true && i != (this.sampleIds.length - 1) ){
              currentItem.active = false;
              var tempObj = this.sampleIds[i+1];
              tempObj.active = true;
              this.objArrow = tempObj;    
              this.focusTable.nativeElement.scrollTop = this.focusTable.nativeElement.scrollTop + 73;      
              break;
            }
        }
        if(tempObj){        
          this.getDataByArrowitem(tempObj);
        }else{
          this.isLoading = false;
        }
      }
      
    }

  }

  ionViewDidLoad() {
    this.focusTable.nativeElement.focus();
    console.log('ionViewDidLoad ResultValidationPage');
    let dataSamples = this.navParams.get('dataSamples');
    let dataListOrderResuls = this.navParams.get('dataListOrderResuls') || [];
    let dataListOpAlert = this.navParams.get('dataListOpAlert') || []
    this.modelSearch = this.navParams.get('modelSearch');
    this.totalSampleId = this.navParams.get('totalSampleId') || 0;
    if (dataSamples != null) {
      this.sampleIds = dataSamples.content || [];
      if (dataSamples.content.length) {
        this.sampleIds[0].active = true;
        this.patientDemoGraphics = this.sampleIds[0];
        this.conditionSearch = this.sampleIds[0].pk;
      }
    }
    if (dataListOrderResuls.length) {
      dataListOrderResuls.forEach(element => {
        // if (element.reflex == 'T') {
        //   this.listOrderResultsManual.push(element);
        // } else {
        //   this.listOrderResultsInstrusment.push(element);
        // }
        this.listOrderResultsInstrusment.push(element);
        
      });
    }
    if (dataListOpAlert.length) {
      this.listOpAlert = dataListOpAlert;
    }
  }

  getDataByArrowitem(item){
    let loader = this.lc.create();
    loader.present();
    this.patientDemoGraphics = item;
    this.listOrderResultsInstrusment = [];
    this.listOrderResultsManual = [];
    this.listOpAlert = [];
    this.conditionSearch = item.pk;
    this.conditionSearch.sort= 'asc';

    this.data.loadOrderResults(item.pk.lPeriod, item.pk.lId, item.pk.orderNumber).subscribe(res => {
      
      let dataListOrderResuls = res.content;
      this.data.loadOpAlert(item.pk.lPeriod, item.pk.lId, item.pk.orderNumber).subscribe(res => {
        if (dataListOrderResuls.length) {
          this.listOrderResultsInstrusment = [];
          dataListOrderResuls.forEach(element => {
            this.listOrderResultsInstrusment.push(element);
          });
        }
        this.listOpAlert = res.content;
        loader.dismiss();
        this.isLoading = false;
      },error =>{
        loader.dismiss();
        this.isLoading = false;
      });
    },error=>{
      loader.dismiss();
      this.isLoading = false;
    });
  }

  getDataSampleId(item) {
    let loader = this.lc.create();
    loader.present();
    this.sampleIds.forEach(element => {
      element.active = false;
    });
    item.active = true;
    this.patientDemoGraphics = item;
    this.listOrderResultsInstrusment = [];
    this.listOrderResultsManual = [];
    this.listOpAlert = [];
    this.conditionSearch = item.pk;
    this.conditionSearch.sort= 'asc';

    this.data.loadOrderResults(item.pk.lPeriod, item.pk.lId, item.pk.orderNumber).subscribe(res => {
      
      let dataListOrderResuls = res.content;
      this.data.loadOpAlert(item.pk.lPeriod, item.pk.lId, item.pk.orderNumber).subscribe(res => {
        if (dataListOrderResuls.length) {
          this.listOrderResultsInstrusment = [];
          dataListOrderResuls.forEach(element => {
            this.listOrderResultsInstrusment.push(element);
          });
        }
        this.listOpAlert = res.content;
        loader.dismiss();
      },error=>{
        loader.dismiss();
      });
    },error=>{
      loader.dismiss();
    });

  }

  private temppage: any;
  changePage(page) {
    this.sampleIds = [];
    this.patientDemoGraphics = {};
    this.listOrderResultsInstrusment = [];
    this.listOrderResultsManual = [];
    this.listOpAlert = [];

    this.data.searchSampleExplorer(this.modelSearch, page.pageIndex + 1, this.fieldSort, this.typeSort).subscribe(res => {
      this.sampleIds = res.content || [];
      this.totalSampleId = res.totalElements;
      if (this.sampleIds.length) {
        this.getDataSampleId(this.sampleIds[0]);
      }
    });
  }
  sort(field) {
    if (this.fieldSort != field) {
      this.fieldSort = field;
      this.typeSort = TypeSort.asc;
    } else {
      switch (this.typeSort) {
        case TypeSort.none: this.typeSort = TypeSort.asc; break;
        case TypeSort.asc: this.typeSort = TypeSort.desc; break;
        case TypeSort.desc:  this.typeSort = TypeSort.none; break;
      }
    }
    console.log(this.fieldSort, this.typeSort);
    //call 
    this.changePage({ pageIndex: 0 });
  }
  toggleOPAlert(){
    this.showOPAlert = !this.showOPAlert;
  }
}

