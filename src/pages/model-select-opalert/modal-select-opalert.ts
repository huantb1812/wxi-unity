import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';
import { DataProvider } from '../../providers/data/data';
import * as _ from 'lodash';

/**
 * Generated class for the ModalSelectMrnPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
    selector: 'page-modal-select-opalert',
    templateUrl: 'modal-select-opalert.html',
})
export class ModalSelectOPAlertPage {
    public arrayOPAlert: Array<any>;
    public tempSelected: Array<any>;
    public totalItems: number;
    public pager: any;
    public itemsSelected: Array<any> = [];
    constructor(public navCtrl: NavController, public navParams: NavParams,
        public viewCtrl: ViewController, private data: DataProvider) {
        this.totalItems = 0;
        this.getData(1);
        this.tempSelected =[];
    }
    getData(page = 1) {
        this.arrayOPAlert = [];
        this.data.getOpAlert(page).subscribe(results => {
            //console.log(results);
            var tempArray = this.navParams.get('mrn');
            if(!this.tempSelected.length){
                if(tempArray){
                    this.tempSelected = tempArray;
                }
            }
            //var testArr = [{"opAlertCode":"RBC002","priority":"G","active":"T","dateModified":"2014-06-11T17:00:00.000+0000","modifyingUser":"WAM","dateCreated":"2014-06-11T17:00:00.000+0000","creatingUser":"WAM","oatext":"RBC Abnormal Distribution: Scan for abnormal RBC morphology"},{"opAlertCode":"RBC003","priority":"G","active":"T","dateModified":"2014-06-11T17:00:00.000+0000","modifyingUser":"WAM","dateCreated":"2014-06-11T17:00:00.000+0000","creatingUser":"WAM","oatext":"RBC Agglutination: Scan slide, follow SOP"},{"opAlertCode":"RBC004","priority":"G","active":"T","dateModified":"2014-06-11T17:00:00.000+0000","modifyingUser":"WAM","dateCreated":"2014-06-11T17:00:00.000+0000","creatingUser":"WAM","oatext":"Turbidity/HGB Interference: Spin HCT, follow SOP"},];
            this.arrayOPAlert = results["content"];

            this.totalItems = results["totalElements"] || 0;
            this.pager = this.data.getPager(this.totalItems, page);
            var self = this;
            this.arrayOPAlert.forEach(function(element){
                element.selected = self.checkSelected(element) ? true : false;
            });
        }, err => {

        });
    }

    public showDetail(item) {
        for (let i = 0; i < this.arrayOPAlert.length; i++) {
          const element = this.arrayOPAlert[i];
          element.selected = false;
        }
        item.selected = true;
        this.tempSelected = item;   
      }

    public checkSelected(item) {
        for (let i = 0; i < this.tempSelected.length; i++) {
          const t = this.tempSelected[i];
          if (t.id == item.id) {
            return true;
          }
        }
        return false;
    }

    closeModal() {
        this.navCtrl.pop();
    }
    ionViewDidLoad() {
        console.log('ionViewDidLoad ModalSelectMrnPage');
    }
    dismiss(data_: Array<any>) {
        this.viewCtrl.dismiss(data_);
    }
    pushData() {
        // let tempArray = new Array<any>();
        // this.arrayMRN.forEach(element => {
        //     if (element["selected"]) {
        //         tempArray.push(element);
        //     }
        // });
        this.dismiss(this.tempSelected);
    }
    add() {
        this.navCtrl.pop().then(() => {
            console.log("selected mrn");
        })
    }
    updateItemsSelected(item) {
        if (item.selected) {
            this.itemsSelected.push(item);
        } else {
            for (let i = 0; i < this.itemsSelected.length; i++) {
                const element = this.itemsSelected[i];
                if (element.testCode == item.testCode) {
                    this.itemsSelected.splice(i, 1);
                    break;
                }
            }
        }
    }
    changeSelection(event) {
        this.updateItemsSelected(event);
        // this.searchModel.testCode = [];
        // for (let i = 0; i < this.itemsSelected.length; i++) {
        //     let item = this.itemsSelected[i];

        //     if (item.selected == true) {
        //         this.searchModel.testCode.push(item);
        //     }
        // }
    }
    changePage(event) {
        this.getData(event.pageIndex);
    }

    checkboxChange(itemSelected){
        if(itemSelected.selected){
            this.tempSelected.push(itemSelected);
        }else{
            var tempArray = _.cloneDeep(this.tempSelected);
            this.tempSelected = _.reject(tempArray, function(o) { return o.id == itemSelected.id; });
        }
    }
}
