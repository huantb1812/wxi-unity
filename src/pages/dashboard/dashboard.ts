import { Component } from '@angular/core';
import { NavController,Events } from 'ionic-angular';
import { DataProvider } from '../../providers/data/data';

/**
 * Generated class for the DashboardPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */


@Component({
  selector: 'page-dashboard',
  templateUrl: 'dashboard.html',
})
export class DashboardPage {
  apps: any[];
  salutation: string;
  user: any;
  tallyCounter: string = 'stat';
  heroPrompt = 'Search for Sample ID';

  boxes: any[];

  constructor(
    private data: DataProvider,
    private navCtrl: NavController,
    private events: Events) {
    this.events.subscribe('USER', this.update);

  }
  ionViewWillEnter() {

  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad DashboardPage');
    this.tallyCounterChanged();
    this.apps = this.data.apps();
  }

  launch(app) {
    if (app.status == 1) {
      this.navCtrl.push(app.page, { user: this.user });
      console.info('launch', app);
    }
  }

  tallyCounterChanged() {
    this.boxes = this.data.tallyCounter(this.tallyCounter == 'stat');
  }

  search(query) {
    console.info('search', query);
    this.navCtrl.push('results', { query: { id: query } });
  }

  tallyBoxClick(box, isStat, index) {
    if (isStat) {
      this.navCtrl.push('results', { priority: 0, status: index });
    } else {
      this.navCtrl.push('results', { status: index });
    }
  }
  update = (user) => {
    console.info('step1: user', user);
    this.user = user;
  }

}
