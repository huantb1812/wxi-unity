import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';


import * as moment from 'moment';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { DataProvider } from '../../providers/data/data';

/**
 * Generated class for the SamplesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */


@Component({
  selector: 'page-samples',
  templateUrl: 'samples.html',
})
export class SamplesPage {
  minDate:Date;
  maxDate:Date;
  fromDate:Date;
  toDate:Date;
  workplaceData:any[];
  searchForm:FormGroup;
  searchPrompt:string = 'Search by Sample ID(s)';
  searchHint:string = 'Sample ID can also be a range, e.g. 12000-12100';

  constructor(private navCtrl: NavController, private navParams: NavParams, private data:DataProvider) {

    this.searchForm = new FormGroup({
      query:new FormControl(null, Validators.compose([Validators.maxLength(512)])),
      queryType:new FormControl('id'),
      fromDate:new FormControl(null),
      toDate:new FormControl(null),
      workplaceSelect:new FormControl(null)
  });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SamplesPage');
    this.minDate = moment().subtract(3, 'years').toDate();
    this.fromDate = moment().subtract(1, 'day').toDate();
    this.toDate = moment().toDate();
    this.maxDate = moment().toDate();
    this.reload();
  }

  reload(){
    this.data.get('assets/data/analyzers.json').subscribe(result=>{
      console.info('analyzers', result);
      this.workplaceData = result;
    }, reason=>{
      console.warn('error', reason);
    })
  }

  queryTypeChanged(){

    let type = this.searchForm.get('queryType').value;
    console.info('queryTypeChanged', type);
    if(type == 'patient_id'){
      this.searchPrompt = 'Search by Medical Record Number';
      this.searchHint = 'Also known as Patient ID';
    }
    else if(type == 'patient_name'){
      this.searchPrompt = 'Search by Patient Name';
      this.searchHint = 'Wildcard values are accepted e.g. Smi*';
    }
    else if(type == 'source_location'){
      this.searchPrompt = 'Search by Request Location';
      this.searchHint = 'Your request location identifiers are configured per location, e.g. Pediatrics, ER';
    }
    else {
      this.searchPrompt = 'Search by Sample ID(s)';
      this.searchHint = 'Sample ID can also be a range, e.g. 12000-12100';
    }
  }

  results(){

  }
  clear(){
    this.searchForm.reset();
  }
  search(){
    this.navCtrl.push('results', {query:this.searchForm.value});
  }

}
