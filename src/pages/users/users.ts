import { Component } from '@angular/core';
import { NavController, AlertController, Events, ModalController } from 'ionic-angular';
import { AuthProvider } from '../../providers/auth/auth';
import { LoginPage } from '../login/login';

/**
 * Generated class for the UsersPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */


@Component({
  selector: 'page-users',
  templateUrl: 'users.html',
})
export class UsersPage {
  users:any[];
  constructor(private ac:AlertController,
              private events:Events,
              private auth:AuthProvider,
              private mc:ModalController,
              private nc: NavController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad UsersPage');
    this.reload();
  }

  pick(u:any) {
    this.nc.pop().then(()=>{
      let modal = this.mc.create(LoginPage, {user:u}, {cssClass:'modal-login', enableBackdropDismiss:false});
      modal.present();
      //this.auth._user = u;
      //this.events.publish('USER', u);
    });
  }

  logout(){
    let modal = this.mc.create(LoginPage, {}, {cssClass:'modal-login', enableBackdropDismiss:false});
    modal.present().then(()=>{
      this.nc.pop();
    });

  }

  reload(){
    console.info('reload');
    this.auth.users(4).subscribe(users=>{
      this.users = users;
    }, reason=>{

    })
  }
}
