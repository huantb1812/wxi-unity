import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController, LoadingController } from 'ionic-angular';
import { ModalSelectMrnPage } from '../modal-select-mrn/modal-select-mrn';
import { ModalSelectPatientNamePage } from '../modal-select-patient-name/modal-select-patient-name';
import { ModalSelectReqLocationPage } from '../modal-select-req-location/modal-select-req-location';
import { ModalSelectWorkPlacePage } from '../modal-select-workplace/modal-select-workplace';
import * as _ from 'lodash';
import { SampleExplorerModel } from '../../providers/model/sampleExplorerModel';
import { DataProvider } from "../../providers/data/data";
import { TypeSort } from '../../providers/model/typeSort';
//import {SampleExplorerModel} from '../../providers/model/sampleExplorerModel';
/**
 * Generated class for the SampleExplorerPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-sample-explorer',
  templateUrl: 'sample-explorer.html',
})
export class SampleExplorerPage {
  public mrnSelectedArray: Array<any>;
  public patientNameSelectedArray: Array<any>;
  public workPlaceSelectedArray: Array<any>;
  public locationSelectedArray: Array<any>;
  public page_resultvalidation: string = "resultvalidation";
  public model: SampleExplorerModel;

  /**
   * clear
   */
  public clear() {
    this.model = new SampleExplorerModel();
    this.mrnSelectedArray = [];
    this.patientNameSelectedArray = [];
    this.workPlaceSelectedArray = [];
    this.locationSelectedArray = [];
  }

  /**
   * search
   */
  public search() {
    let loader = this.lc.create();
    loader.present();
    let type = TypeSort.none;

    this.data.searchSampleExplorer(this.model, 1, '', type).subscribe(res => {
      let totalSampleId = res.totalElements;
      let sampleIds = res.content || [];
      let dataSamples = res;
      if (sampleIds.length) {
        let sampleResultsFirst = sampleIds[0];
        this.data.loadOrderResults(sampleResultsFirst.pk.lPeriod, sampleResultsFirst.pk.lId,
          sampleResultsFirst.pk.orderNumber).subscribe(res => {
            let dataListOrderResuls = res.content;
            this.data.loadOpAlert(sampleResultsFirst.pk.lPeriod, sampleResultsFirst.pk.lId,
              sampleResultsFirst.pk.orderNumber).subscribe(res => {
                console.log(res);
                let dataListOpAlert = res.content;
                this.navCtrl.push(this.page_resultvalidation, { dataSamples: dataSamples, modelSearch: this.model, totalSampleId: totalSampleId, dataListOrderResuls: dataListOrderResuls, dataListOpAlert: dataListOpAlert });
                loader.dismiss();
              });
          });
        loader.dismiss();
      } else {
        this.navCtrl.push(this.page_resultvalidation, { dataSamples: dataSamples, modelSearch: this.model, totalSampleId: totalSampleId });
        loader.dismiss();
      }
    });
  }
  constructor(public navCtrl: NavController, public navParams: NavParams, private mc: ModalController, private data: DataProvider, private lc: LoadingController) {
    this.model = new SampleExplorerModel();
  }
  showMrn() {
    let currentDistinct = this.model.mrn.split('; ') || [];
    this.mrnSelectedArray = [];
    currentDistinct.forEach(item => {
      if (item != "") {
        this.mrnSelectedArray.push({ id: item });
      }
    });

    let modal = this.mc.create(ModalSelectMrnPage, { mrn: _.cloneDeep(this.mrnSelectedArray) }, { enableBackdropDismiss: false, cssClass: 'modal-mrn' });
    modal.onDidDismiss(data => {
      // this.mrnSelectedArray = data;
      // if (data) {
      //   this.mrnSelectedArray = data;
      //   var tempArray = _.map(data, 'id');
      //   this.model.mrn = tempArray.filter(function (val) { return val; }).join(';');
      // }
      if(data){
        let kipItems = _.filter(data, (v) => {
          return v.selected == true;
        }) || [];
        if (kipItems.length > 0) {
  
          let arrAdd = _.map(kipItems, 'id') || [];//arr1
          this.model.mrn = arrAdd.join('; ');
        }else{
          this.model.mrn = "";
        }
      }

    });
    modal.present();
  }
  showPatientNames() {

    let currentDistinct = this.model.patientName.split('; ') || [];
    this.patientNameSelectedArray = [];
    currentDistinct.forEach(item => {
      if (item != "") {
        this.patientNameSelectedArray.push({ id: item });
      }
    });

    let modal = this.mc.create(ModalSelectPatientNamePage, { patientName: _.cloneDeep(this.patientNameSelectedArray) }, { enableBackdropDismiss: false, cssClass: 'modal-mrn' });
    modal.onDidDismiss(data => {
      // if (data) {
      //   this.patientNameSelectedArray = data;
      //   var tempArray = _.map(data, 'name');
      //   this.model.patientName = tempArray.filter(function (val) { return val; }).join(';');
      // }
      if (data) {
        let kipItems = _.filter(data, (v) => {
          return v.selected == true;
        }) || [];
        if (kipItems.length > 0) {
          let arrAdd = _.map(kipItems, 'id') || [];//arr1
          this.model.patientName = arrAdd.join('; ');
        }else{
          this.model.patientName = "";
        }
      }
    });
    modal.present();
  }
  showLocation() {
    let currentDistinct = this.model.requestingLocation.split('; ') || [];
    this.locationSelectedArray = [];
    currentDistinct.forEach(item => {
      if (item != "") {
        this.locationSelectedArray.push({ location: item });
      }
    });

    let modal = this.mc.create(ModalSelectReqLocationPage, { location: _.cloneDeep(this.locationSelectedArray) }, { enableBackdropDismiss: false, cssClass: 'modal-mrn' });
    modal.onDidDismiss(data => {
    if(data){
      let kipItems = _.filter(data, (v) => {
        return v.selected == true;
      }) || [];
      if (kipItems.length > 0) {
        let arrAdd = _.map(kipItems, 'location') || [];//arr1
        this.model.requestingLocation = arrAdd.join('; ');
      }else{
        this.model.requestingLocation = "";
      }
    }
    });
    modal.present();
  }

  showWorkPlace() {
    let currentDistinct = this.model.workplace.split('; ') || [];
    this.workPlaceSelectedArray = [];
    currentDistinct.forEach(item => {
      if (item != "") {
        this.workPlaceSelectedArray.push({ workplace: item });
      }
    });

    let modal = this.mc.create(ModalSelectWorkPlacePage, { workplace: this.workPlaceSelectedArray }, { enableBackdropDismiss: false, cssClass: 'modal-mrn' });
    modal.onDidDismiss(data => {
      if(data){
        let kipItems = _.filter(data, (v) => {
          return v.selected == true;
        }) || [];
        if (kipItems.length>0) {
          let arrAdd = _.map(kipItems, 'workplace') || [];//arr1
          this.model.workplace = arrAdd.join('; ');
        }else{
          this.model.workplace = "";
        }
      }
    });
    modal.present();
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad SampleExplorerPage');
  }
  deleteMRN(index) {
    this.mrnSelectedArray.splice(index, 1);
    var tempArray = _.map(this.mrnSelectedArray, 'id');
    this.model.mrn = tempArray.filter(function (val) { return val; }).join('; ');
  }
  deletePatientName(index) {
    this.patientNameSelectedArray.splice(index, 1);
    var tempArray = _.map(this.patientNameSelectedArray, 'name');
    this.model.patientName = tempArray.filter(function (val) { return val; }).join('; ');
  }
  deleteLocation(index) {
    this.locationSelectedArray.splice(index, 1);
    var tempArray = _.map(this.locationSelectedArray, 'location');
    this.model.requestingLocation = tempArray.filter(function (val) { return val; }).join('; ');
  }
  deleteWorkPlace(index) {
    this.workPlaceSelectedArray.splice(index, 1);
    var tempArray = _.map(this.workPlaceSelectedArray, 'workplace');
    this.model.workplace = tempArray.filter(function (val) { return val; }).join('; ');
  }
}
