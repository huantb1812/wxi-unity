import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';
import { DataProvider } from '../../providers/data/data';
import * as _ from 'lodash';
/**
 * Generated class for the ModalSelectPatientNamePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
    selector: 'page-modal-select-patient-name',
    templateUrl: 'modal-select-patient-name.html',
})
export class ModalSelectPatientNamePage {
    public arrayPatientName: Array<any>;
    public pager: any;
    public totalItems: any;
    public tempSelected: Array<any>;
    constructor(public navCtrl: NavController, public navParams: NavParams, public viewCtrl: ViewController, private data: DataProvider) {
        this.getData(1);
        this.tempSelected = [];
    }

    public checkSelected(item) {
        for (let i = 0; i < this.tempSelected.length; i++) {
            const t = this.tempSelected[i];
            if (t.id == item.id) {
                return true;
            }
        }
        return false;
    }

    checkboxChange(item) {
        if (item.selected) {
            this.tempSelected.push(item);
        } else {
            // var tempArray = _.cloneDeep(this.tempSelected);
            // this.tempSelected = _.reject(tempArray, function(o) { return o.id == item.id; });
            if (item.old) {
                let t = _.find(this.tempSelected, (v) => {
                    return v.id == item.id;
                }) || null;
                if (t) {
                    t.selected = false;
                }
            } else {
                this.tempSelected = _.reject(this.tempSelected, function (o) { return o.id == item.id; });
            }
        }
    }

    getData(page = 1) {
        this.arrayPatientName = [];
        this.data.getMrnAndName(page).subscribe(results => {
            //console.log(results);
            var tempArray = this.navParams.get('patientName');
            if (!this.tempSelected.length) {
                if (tempArray) {
                    tempArray.forEach(element => {
                        this.tempSelected.push({ id: element.id, old: true, selected: true });
                    });
                }
            }
            this.arrayPatientName = results["content"];
            this.totalItems = results["totalElements"] || 0;
            this.pager = this.data.getPager(this.totalItems, page);
            var self = this;
            this.arrayPatientName.forEach(function (element) {
                element.selected = self.checkSelected(element) ? true : false;
                element.old = element.selected ? true : false;
            });
        }, err => {

        });
    }

    closeModal() {
        this.navCtrl.pop();
        //this.dismiss();        
    }
    ionViewDidLoad() {
        console.log('ionViewDidLoad ModalSelectMrnPage');
    }
    dismiss(data_: Array<any>) {
        //let data_ = { 'foo': 'bar' };
        this.viewCtrl.dismiss(data_);
    }
    pushData() {

        this.dismiss(this.tempSelected);
    }
    add() {
        this.navCtrl.pop().then(() => {
            console.log("selected mrn");
        })
    }
    changePage(event) {
        this.getData(event.pageIndex);
    }

}
