import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';
import { DataProvider } from '../../providers/data/data';
import * as _ from 'lodash';
/**
 * Generated class for the ModalSelectMrnPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
    selector: 'page-modal-select-mrn',
    templateUrl: 'modal-select-mrn.html',
})
export class ModalSelectMrnPage {
    public arrayMRN: Array<any>;
    public tempSelected: Array<any>;
    public totalItems: number;
    public pager: any;
    public itemsSelected: Array<any> = [];
    constructor(public navCtrl: NavController, public navParams: NavParams,
        public viewCtrl: ViewController, private data: DataProvider) {
        this.totalItems = 0;
        this.getData(1);
        this.tempSelected = [];
    }
    getData(page = 1) {
        this.arrayMRN = [];
        this.data.getMrnAndName(page).subscribe(results => {
            //console.log(results);
            var tempArray = this.navParams.get('mrn');
            if (!this.tempSelected.length) {
                if (tempArray) {
                    tempArray.forEach(element => {
                        this.tempSelected.push({ id: element.id, old: true, selected: true });
                    });
                }
            }

            this.arrayMRN = results["content"];

            this.totalItems = results["totalElements"] || 0;
            this.pager = this.data.getPager(this.totalItems, page);
            var self = this;
            this.arrayMRN.forEach(function (element) {
                element.selected = self.checkSelected(element) ? true : false;
                element.old = element.selected ? true : false;
            });
        }, err => {

        });
    }

    public checkSelected(item) {
        for (let i = 0; i < this.tempSelected.length; i++) {
            const t = this.tempSelected[i];
            if (t.id == item.id) {
                return true;
            }
        }
        return false;
    }

    closeModal() {
        this.navCtrl.pop();
    }
    ionViewDidLoad() {
        console.log('ionViewDidLoad ModalSelectMrnPage');
    }
    dismiss(data_: Array<any>) {
        this.viewCtrl.dismiss(data_);
    }
    pushData() {
      
        this.dismiss(this.tempSelected);
    }
    add() {
        this.navCtrl.pop().then(() => {
            console.log("selected mrn");
        })
    }
    updateItemsSelected(item) {
        if (item.selected) {
            this.itemsSelected.push(item);
        } else {
            for (let i = 0; i < this.itemsSelected.length; i++) {
                const element = this.itemsSelected[i];
                if (element.testCode == item.testCode) {
                    this.itemsSelected.splice(i, 1);
                    break;
                }
            }
        }
    }
    changeSelection(event) {
        this.updateItemsSelected(event);
    }
    changePage(event) {
        this.getData(event.pageIndex);
    }

    checkboxChange(item) {
        if (item.selected) {
            this.tempSelected.push(item);
        } else {
            // var tempArray = _.cloneDeep(this.tempSelected);
            if (item.old) {
                let t = _.find(this.tempSelected, (v) => {
                    return v.id == item.id;
                }) || null;
                if (t) {
                    t.selected = false;
                }
            } else {
                this.tempSelected = _.reject(this.tempSelected, function (o) { return o.id == item.id; });
            }
        }
    }
}
