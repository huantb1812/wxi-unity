import { Component, ViewChild } from '@angular/core';
import { Tabs } from 'ionic-angular';

import { AlertsPage } from '../alerts/alerts';
import { DashboardPage } from '../dashboard/dashboard';
import { SamplesPage } from '../samples/samples';
import { SettingsPage } from '../settings/settings';

import * as _ from 'lodash';
import { SampleExplorerPage } from '../sample-explorer/sample-explorer';

/**
 * Generated class for the TabsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-tabs',
  templateUrl: 'tabs.html',
})
export class TabsPage {
  @ViewChild('tab') tabRef: Tabs;
  private initialIndex = 0;
  private navPages = [
    { label: 'Dashboard', page: 'dashboard', component: DashboardPage },
    // {label:'Sample Explorer', page:'samples', component:SamplesPage},
    { label: 'Sample Explorer', page: 'sampleexplorer', component: SampleExplorerPage },
    { label: 'Alerts', page: 'alerts', component: AlertsPage },
    { label: 'Settings', page: 'settings', component: SettingsPage }
  ];
  constructor() {

    let index = _.findIndex(this.navPages, function (o) { return location.hash.indexOf(o.page) > -1 });
    this.initialIndex = index < 0 ? 0 : index;
    console.info('TABS: INIT', this.initialIndex, location.hash);

  }
  ionViewDidLoad() {
    if (this.initialIndex > 0) {
      this.tabRef.select(this.initialIndex);
    }
  }

}
