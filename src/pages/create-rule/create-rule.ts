import { Component, ViewChild } from '@angular/core';
import { NavController, NavParams, AlertController } from 'ionic-angular';
import { CreateRuleModel } from '../../providers/model/createRuleModel';
import { DataProvider } from '../../providers/data/data';
import { TestLevelSelectionComponent } from '../../components/test-level-selection/test-level-selection';

@Component({
  selector: 'page-create-rule',
  templateUrl: 'create-rule.html',
})
export class CreateRulePage {
  public isUpdate: boolean;
  public searchModel: any = {};
  public createModel: CreateRuleModel;
  public messages: Array<any>;
  
  @ViewChild(TestLevelSelectionComponent) testLevelSelectionComponent: TestLevelSelectionComponent;

  constructor(public navCtrl: NavController, public navParams: NavParams, public data: DataProvider, private alertCtrl: AlertController) {
    this.createModel = new CreateRuleModel();
    this.isUpdate = false;
    this.messages = [];
  }

  ionViewDidLoad() {
    let t = this.navParams.get('rule');
    this.isUpdate = this.navParams.get('isUpdate');
    this.createModel.wrapInput(t);

  }

  update() {
 
    this.messages = this.createModel.valid();
    if (this.messages.length > 0) {
      var self = this;
      this.messages.forEach(function(obj){
        if(obj.field == "ruleName"){
          self.createModel.checkValid.invalidRuleName = true;
        }else{
          self.createModel.checkValid.invalidRuleName = false;
        }
      });
      return;
    }
    else {
      let data = this.createModel.wrapOutput();
       if (this.isUpdate) {
        data.ruleId = this.isUpdate ? this.createModel.ruleId : null;
        this.data.updaterule(data).subscribe(res => {
          console.log('update rule,', this.createModel);
          let alert = this.alertCtrl.create({
            title: '',
            subTitle: 'Record was saved successful.',
            buttons: ['OK']
          });
          alert.present();
          //alert('Record was saved successful');
          this.navCtrl.pop();
        },error=>{
          console.log(error);
          if(error.status == 400 && error.error.indexOf("ruleName must be unique")){
            var checkUnique = [{field: 'ruleName', message:'Rule Name has existed in the system.'}];
            this.messages= checkUnique; 
            this.createModel.checkValid.invalidRuleName = true;
          }
          
        })
      } else {
        this.data.saverule(data).subscribe(res => {
          console.log('create new rule,', this.createModel);
          let alert = this.alertCtrl.create({
            title: '',
            subTitle: 'Record was saved successful.',
            buttons: ['OK']
          });
          alert.present();
          //alert('Record was saved successful');
          this.navCtrl.pop();
        },error=>{
          console.log(error);
          if(error.status == 400 && error.error.indexOf("ruleName must be unique")){
            var checkUnique = [{field: 'ruleName', message:'Rule Name has existed in the system.'}];
            this.messages= checkUnique; 
            this.createModel.checkValid.invalidRuleName = true;
          }
          
        })
      }

    }
  }
  cancel() {
    this.navCtrl.pop();
  }
}
