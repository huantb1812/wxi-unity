import { Component } from '@angular/core';
import { NavController, NavParams, AlertController, LoadingController } from 'ionic-angular';
import { DataProvider } from '../../providers/data/data';
import * as _ from 'lodash';
@Component({
  selector: 'page-rule-follow-difinition',
  templateUrl: 'rule-follow-difinition.html',
})
export class RuleFollowDifinitionPage {
  loading: boolean;
  ruleFollows: any;
  constructor(public navCtrl: NavController, public navParams: NavParams, private data: DataProvider,private alertCtrl: AlertController, private lc: LoadingController) {
    this.ruleFollows = {
      content: [],
      totalItems: 0
    }
    this.loading = false;
    
  }

  getRuleFollow() {
    let loader = this.lc.create();
    loader.present();
    this.loading = true;
    this.data.getRuleFollows().subscribe(res => {
      if (res.length > 0) {
        res.forEach(item => {
          item.listRule = (item.ruleFlowDetails.map((v, i, a) => {
            return v.ruleName;
          }) || []).join(', ');
        });
      }
      this.ruleFollows = {
        content: res,
        totalItems: res.length
      };
      for(var i=0; i< this.ruleFollows.content.length; i++){
        var temp = this.ruleFollows.content[i];
        temp.isShow =true;
      }
      this.loading = false;
      loader.dismiss();
    }, err => {
      this.loading = false;
      loader.dismiss();
    });
  }
  add(rulefollow) {
    this.navCtrl.push('modalruleflowadd', { rulefollow: rulefollow });
  }
  showDetail(item) {
    item.isShow = true;
  }
  hiddenDetail(item) {
    item.isShow = false;
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RuleFlowDifinitionPage');
    
  }
  ionViewWillEnter(){
    console.log(1);
    this.getRuleFollow();
  }
  remove(rulefollow, item, index) {
    rulefollow.ruleFlowDetails.splice(index, 1);
  }
  sort_up(rulefollow, item, index) {
    if (index == 0) return;
    this.data.array_move(rulefollow.ruleFlowDetails, index, index - 1)
  }
  sort_down(rulefollow, item, index) {

    if (index == rulefollow.ruleFlowDetails.length - 1) return;

    this.data.array_move(rulefollow.ruleFlowDetails, index, index + 1)
  }
  update_rule_follow(rulefollow) {

    let data = {
      ruleFlowId: rulefollow.ruleFlowId,
      ruleFlowName: rulefollow.ruleFlowName,
      ruleFlowDetails: rulefollow.ruleFlowDetails,
      dateModified: rulefollow.dateModified,
      modifyingUser: rulefollow.modifyingUser,
      dateCreated: rulefollow.dateCreated,
      creatingUser: rulefollow.creatingUser
    }
    for (let i = 0; i < data.ruleFlowDetails.length; i++) {
      data.ruleFlowDetails[i].orderInFlow = i + 1;
    }

    this.data.updateRuleFollowDifinition(rulefollow).subscribe(res => {
      rulefollow = res;
      let alert = this.alertCtrl.create({
        title: '',
        message: 'Saved success.',
        buttons: ['OK']
      });
      alert.present();
      this.getRuleFollow();
    });
  }

}
