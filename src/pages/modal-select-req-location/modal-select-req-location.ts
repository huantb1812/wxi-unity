import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';
import { DataProvider } from '../../providers/data/data';
import * as _ from 'lodash';

/**
 * Generated class for the ModalSelectReqLocationPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
    selector: 'page-modal-select-req-location',
    templateUrl: 'modal-select-req-location.html',
})
export class ModalSelectReqLocationPage {
    public arrayLocation: Array<any>;
    public tempSelected: Array<any>;
    public totalItems: number;
    public pager: any;

    constructor(public navCtrl: NavController, public navParams: NavParams, public viewCtrl: ViewController, private data: DataProvider) {
        this.getData(1);
        this.tempSelected = [];
    }
    getData(page = 1) {
        this.arrayLocation = [];
        this.data.getLocation(page).subscribe(results => {
            //console.log(results);
            var tempArray = this.navParams.get('location');
            if (!this.tempSelected.length) {
                if (tempArray) {
                    tempArray.forEach(element => {
                        this.tempSelected.push({ location: element.location, old: true, selected: true });
                    });
                }
            }
            this.arrayLocation = results["content"];
            this.totalItems = results["totalElements"] || 0;
            this.pager = this.data.getPager(this.totalItems, page);
            var self = this;
            this.arrayLocation.forEach(function (element) {
                element.selected = self.checkSelected(element) ? true : false;
                element.old = element.selected ? true : false;

            });
        }, err => {

        });
    }
    public checkSelected(item) {
        for (let i = 0; i < this.tempSelected.length; i++) {
            const t = this.tempSelected[i];
            if (t.location == item.location) {
                return true;
            }
        }
        return false;
    }
    checkboxChange(item) {
        if (item.selected) {
            this.tempSelected.push(item);
        } else {
            // var tempArray = _.cloneDeep(this.tempSelected);
            // this.tempSelected = _.reject(tempArray, function(o) { return o.location == item.location; });
            if (item.old) {
                let t = _.find(this.tempSelected, (v) => {
                    return v.location == item.location;
                }) || null;
                if (t) {
                    t.selected = false;
                }
            } else {
                this.tempSelected = _.reject(this.tempSelected, function (o) { return o.location == item.location; });
            }
        }
    }
    closeModal() {
        this.navCtrl.pop();
        //this.dismiss();        
    }
    ionViewDidLoad() {
        console.log('ionViewDidLoad ModalSelectMrnPage');
    }
    dismiss(data_: Array<any>) {
        //let data_ = { 'foo': 'bar' };
        this.viewCtrl.dismiss(data_);
    }
    pushData() {
        // let tempArray = new Array<any>();
        // this.arrayLocation.forEach(element => {
        //     if (element["selected"]) {
        //         //console.log(element);
        //         tempArray.push(element);
        //     }
        // });
        debugger;
        this.dismiss(this.tempSelected);
    }
    add() {
        this.navCtrl.pop().then(() => {
            console.log("selected mrn");
        })
    }
    changePage(event) {
        this.getData(event.pageIndex);
    }

}
