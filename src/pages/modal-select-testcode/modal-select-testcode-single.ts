import { Component, Input } from '@angular/core';
import { SearchModel } from '../../providers/model/searchModel';
import { DataProvider } from "../../providers/data/data";
import {  NavController, NavParams, ModalController, ViewController } from 'ionic-angular';

@Component({
  selector: 'popup-test-code-single',
  templateUrl: 'modal-select-testcode-single.html'
})
export class TestCodeSingleComponent {
  @Input() searchModel: SearchModel;
  public testCodeSelected: Array<any> = [];
  public pager: any;
  public totalItems: any;
  public tempSelected : Array<any>;

  constructor(public navCtrl: NavController, public navParams: NavParams,
    public viewCtrl: ViewController, private data: DataProvider) {
    this.searchModel = new SearchModel();
    this.testCodeSelected = [];
  }
  public testcodes: Array<any> = [];


  ngOnInit() {
    // this.data.getTestCode().subscribe(res => {
    //   this.testcodes = res["content"] || [];

    // });
    this.getData(1);
  }
  checkSelected(item) {
    for (let i = 0; i < this.testCodeSelected.length; i++) {
      const t = this.testCodeSelected[i];
      if (t.testCode == item.testCode) {
        return true;
      }
    }
    return false;
  }


  getData(page = 1) {
    this.testcodes = [];
    this.data.getTestCode(page).subscribe(results => {

      var tempValue = this.navParams.get('testcode');
      // if(!this.testCodeSelected.length){
      //     if(tempArray){
      //         this.testCodeSelected = tempArray;
      //     }else{
      //       this.testCodeSelected = [];
      //     }
      // }

      this.testcodes = results["content"] || [];
      this.totalItems = results["totalElements"] || 0;
      this.pager = this.data.getPager(this.totalItems, page);
      for (let i = 0; i < this.testcodes.length; i++) {
        if(this.testcodes[i].testCode == tempValue){
          this.testcodes[i].selected = true;
          this.testCodeSelected = this.testcodes[i];
        }
      }
    }, err => {

    });
  }
  updateTestCodeSelected(item) {
    if (item.selected) {
      this.testCodeSelected.push(item);
    } else {
      for (let i = 0; i < this.testCodeSelected.length; i++) {
        const element = this.testCodeSelected[i];
        if (element.testCode == item.testCode) {
          this.testCodeSelected.splice(i, 1);
          break;
        }
      }
    }
  }
  public changeSelectionTestCode(event) {
    this.updateTestCodeSelected(event);
    this.searchModel.testCode = [];
    for (let i = 0; i < this.testCodeSelected.length; i++) {
      let item = this.testCodeSelected[i];

      if (item.selected == true) {
        this.searchModel.testCode.push(item);
      }
    }
  }
  public clearSelection() {
    this.searchModel.testCode = [];
    this.testCodeSelected=[];
    for (let i = 0; i < this.testcodes.length; i++) {
      this.testcodes[i].selected = false;
    }
  }
  changePage(event) {
    this.getData(event.pageIndex);
  }
  closeModal() {
    this.navCtrl.pop();
  }

  dismiss(data_: Array<any>) {
    this.viewCtrl.dismiss(data_);
  }
  pushData() {
    this.dismiss(this.testCodeSelected);
  }
  public showDetail(item) {
    for (let i = 0; i < this.testcodes.length; i++) {
      const element = this.testcodes[i];
      element.selected = false;
    }
    item.selected = true;
    this.testCodeSelected = item;   
  }
}
