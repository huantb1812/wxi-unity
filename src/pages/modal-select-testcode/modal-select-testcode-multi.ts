import { Component, Input } from '@angular/core';
import { SearchModel } from '../../providers/model/searchModel';
import { DataProvider } from "../../providers/data/data";
import { IonicPage, NavController, NavParams, ModalController, ViewController } from 'ionic-angular';
import * as _ from 'lodash';

@Component({
  selector: 'popup-test-code-multi',
  templateUrl: 'modal-select-testcode-multi.html'
})
export class TestCodeMultiComponent {
  @Input() searchModel: SearchModel;
  public testCodeSelected: Array<any> = [];
  public pager: any;
  public totalItems: any;
  public tempSelected: Array<any>;

  constructor(public navCtrl: NavController, public navParams: NavParams,
    public viewCtrl: ViewController, private data: DataProvider) {
    this.searchModel = new SearchModel();
    this.testCodeSelected = [];
  }
  public testcodes: Array<any> = [];


  ngOnInit() {
    // this.data.getTestCode().subscribe(res => {
    //   this.testcodes = res["content"] || [];

    // });
    this.getData(1);
  }
  checkSelected(item) {
    for (let i = 0; i < this.testCodeSelected.length; i++) {
      const t = this.testCodeSelected[i];
      if (t.testCode == item.testCode) {
        return true;
      }
    }
    return false;
  }

  getData(page = 1) {
    this.testcodes = [];
    this.data.getTestCode(page).subscribe(results => {

      var tempArray = this.navParams.get('testcode');
      if (!this.testCodeSelected.length) {
        if (tempArray) {
          // this.testCodeSelected = tempArray;
          if (tempArray) {
            tempArray.forEach(element => {
              this.testCodeSelected.push({ testCode: element.testCode, old: true, selected: true });
            });
          }
        }
        //  else {
        //   this.testCodeSelected = [];
        // }
      }

      this.testcodes = results["content"] || [];
      this.totalItems = results["totalElements"] || 0;
      this.pager = this.data.getPager(this.totalItems, page);
      for (let i = 0; i < this.testcodes.length; i++) {
        const item = this.testcodes[i];
        item.selected = this.checkSelected(item) ? true : false;
        item.old = item.selected ? true : false;

      }
    }, err => {

    });
  }
  updateTestCodeSelected(item) {
    if (item.selected) {
      this.testCodeSelected.push(item);
    } else {
      // for (let i = 0; i < this.testCodeSelected.length; i++) {
      //   const element = this.testCodeSelected[i];
      //   if (element.testCode == item.testCode) {
      //     this.testCodeSelected.splice(i, 1);
      //     break;
      //   }
      // }
      if (item.old) {
        let t = _.find(this.testCodeSelected, (v) => {
          return v.testCode == item.testCode;
        }) || null;
        if (t) {
          t.selected = false;
        }
      } else {
        this.testCodeSelected = _.reject(this.testCodeSelected, function (o) { return o.testCode == item.testCode; });
      }
    }
  }
  public changeSelectionTestCode(event) {
    this.updateTestCodeSelected(event);
    // this.searchModel.testCode = [];
    // for (let i = 0; i < this.testCodeSelected.length; i++) {
    //   let item = this.testCodeSelected[i];

    //   if (item.selected == true) {
    //     this.searchModel.testCode.push(item);
    //   }
    // }
  }
  public clearSelection() {
    this.searchModel.testCode = [];
    this.testCodeSelected = [];
    for (let i = 0; i < this.testcodes.length; i++) {
      this.testcodes[i].selected = false;
    }
  }
  changePage(event) {
    this.getData(event.pageIndex);
  }
  closeModal() {
    this.navCtrl.pop();
  }

  dismiss(data_: Array<any>) {
    this.viewCtrl.dismiss(data_);
  }
  pushData() {
    debugger;
    this.dismiss(this.testCodeSelected);
  }
}
