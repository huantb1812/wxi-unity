import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController } from 'ionic-angular';
import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { DataProvider } from '../../providers/data/data';

import {Observable} from 'rxjs/Observable';
import {merge} from 'rxjs/observable/merge';
import {of as observableOf} from 'rxjs/observable/of';
import {catchError} from 'rxjs/operators/catchError';
import {map} from 'rxjs/operators/map';
import {startWith} from 'rxjs/operators/startWith';
import {switchMap} from 'rxjs/operators/switchMap';


import * as _ from 'lodash';
import * as moment from 'moment';

/**
 * Generated class for the SampleResultsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage({
  name:'results',
  segment:'results'
})
@Component({
  selector: 'page-sample-results',
  templateUrl: 'sample-results.html',
})

export class SampleResultsPage {
  queryDigest:any;
  displayedColumns = ['id', 'sample_date', 'priority', 'status'];
  sampleDetailsColumns = ['test_code', 'test_result'];
  dataSource = new MatTableDataSource();
  sampleDetailsDataSource = new MatTableDataSource();
  sample:any;
  resultsLength = 0;
  isLoadingResults = true;
  isRateLimitReached = false;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;


  constructor(private navCtrl: NavController, private navParams: NavParams,
              private data:DataProvider, private lc:LoadingController) {
  }

  ionViewDidLoad() {
    // If the user changes the sort order, reset back to the first page.
    //this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0);

    console.log('ionViewDidLoad SampleResultsPage');
    this.dataSource.sort = this.sort;
    this.reload().subscribe((result:any[])=>{
      if(result[0]){
        result[0].selected = true;
        this.sample = result[0];
      }
      this.dataSource.data = result;
    })
  }
  selectRow(row){
    _.map(this.dataSource.data, (o)=>{o.selected = false; return o;})
    row.selected = true;
    this.sample = row;
  }

  reload(){
    this.loadSampleDetails();
    return new Observable((observer)=>{
      this.queryDigest = {} as any;
      let query = this.navParams.get('query');

      let status = isNaN(this.navParams.get('status'))?null:parseInt(this.navParams.get('status'));
      let priority = isNaN(this.navParams.get('priority'))?null:parseInt(this.navParams.get('priority'));

      console.info('results', query, status, priority);
      let loader = this.lc.create();
      this.data.results().subscribe(result=>{
        //console.info('results', result);
        if(status!=null) {
          this.queryDigest.status = status;
          result = _.filter(result, ['status', status]);
        }
        if(priority!=null) {
          this.queryDigest.priority = priority;
          result = _.filter(result, ['priority', priority]);
        }
        if(query) {
          if(query.fromDate && query.toDate) {
            this.queryDigest.fromDate = moment(query.fromDate).format('MM/DD/YYYY');
            this.queryDigest.toDate = moment(query.toDate).format('MM/DD/YYYY');
            result = _.filter(result, (o)=>{
              return moment(o.sample_date).isBetween(query.fromDate, query.toDate);
          });
        }
        let params = ['id','patient_id','patient_name','source_location'];
        params.forEach(element => {
          if(query[element] != null){
            this.queryDigest.queryType = element;
            result = _.filter(result, (o)=>{
              if(query.id.indexOf('-') > -1 && element == 'id') {
                //sample range check
                let values = query.id.split('-');
                let min = parseInt(values[0]);
                let max = parseInt(values[1]);

                if(!isNaN(min) || !isNaN(max)) {
                  console.info('range', min, o.id, max);
                  return min <= o.id && o.id <= max;
                }
              }
              console.info(element, query[element], o[element].toString());
              return query[element].toString().indexOf(o[element].toString()) > -1;
            });
          }
        });
      }
      console.info('filter result', result, this.queryDigest);
      observer.next(result);
      observer.complete();
      loader.dismiss();
    }, reason=>{
      console.error('Failed to load results', reason);
      observer.error(reason);
      loader.dismiss();
    })
    });



  }

  loadSampleDetails(){
    let loader = this.lc.create();
    this.data.get('assets/data/results-details.json').subscribe(result=>{
      this.sampleDetailsDataSource.data = result;
      loader.dismiss();
    }, reason=>{
      loader.dismiss();
    })
  }



}
