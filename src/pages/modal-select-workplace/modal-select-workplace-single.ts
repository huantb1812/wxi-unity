import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';
import { DataProvider } from '../../providers/data/data';
import * as _ from 'lodash';

/**
 * Generated class for the ModalSelectReqLocationPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
    selector: 'page-modal-select-workplace-single',
    templateUrl: 'modal-select-workplace-single.html',
})
export class ModalSelectSingleWorkPlacePage {
    public arrayWorkPlace: Array<any>;        
    public tempSelected: Array<any>;
    public pager: any;
    public totalItems: any;
    constructor(public navCtrl: NavController, public navParams: NavParams, public viewCtrl: ViewController, private data: DataProvider) {
        this.getData(1);
        this.tempSelected =[];
    }
    getData(page = 1) {
        this.arrayWorkPlace = [];
        this.data.getWorkPlace(page).subscribe(results => {
            var tempValue = this.navParams.get('instr');
            // if(!this.tempSelected){
            //     if(tempArray){
            //         this.tempSelected = tempArray;
            //     }
            // }
            this.arrayWorkPlace = results["content"];
            this.totalItems = results["totalElements"] || 0;
            this.pager = this.data.getPager(this.totalItems, page);
            var self = this;
            this.arrayWorkPlace.forEach(function(element){
                if(element.workplace == tempValue){
                    element.selected = true;
                    self.tempSelected = element;
                }
                // element.selected = self.checkSelected(element) ? true : false;
            });
        }, err => {

        });
    }
    public checkSelected(item) {
        for (let i = 0; i < this.tempSelected.length; i++) {
          const t = this.tempSelected[i];
          if (t.workplace == item.workplace) {
            return true;
          }
        }
        return false;
    }
    checkboxChange(itemSelected){
        if(itemSelected.selected){
            this.tempSelected.push(itemSelected);
        }else{
            var tempArray = _.cloneDeep(this.tempSelected);
            this.tempSelected = _.reject(tempArray, function(o) { return o.workplace == itemSelected.workplace; });
        }
    }
    closeModal() {
        this.navCtrl.pop();
        //this.dismiss();        
    }
    ionViewDidLoad() {
        console.log('ionViewDidLoad ModalSelectMrnPage');
    }
    dismiss(data_: Array<any>) {
        //let data_ = { 'foo': 'bar' };
        this.viewCtrl.dismiss(data_);
    }
    pushData() {
        // let tempArray = new Array<any>();
        // this.arrayWorkPlace.forEach(element => {
        //     if (element["selected"]) {
        //         //console.log(element);
        //         tempArray.push(element);
        //     }
        // });
        this.dismiss(this.tempSelected);
    }
    add() {
        this.navCtrl.pop().then(() => {
            console.log("selected mrn");
        })
    }
    changePage(event) {
        this.getData(event.pageIndex);
    }
    public showDetail(item) {
        for (let i = 0; i < this.arrayWorkPlace.length; i++) {
          const element = this.arrayWorkPlace[i];
          element.selected = false;
        }
        item.selected = true;
        this.tempSelected = item;   
      }
}
