import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';
import { DataProvider } from '../../providers/data/data';
import * as _ from 'lodash';

/**
 * Generated class for the ModalSelectReqLocationPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
    selector: 'page-modal-select-workplace',
    templateUrl: 'modal-select-workplace.html',
})
export class ModalSelectWorkPlacePage {
    public arrayWorkPlace: Array<any>;
    public tempSelected: Array<any>;
    public pager: any;
    public totalItems: any;
    constructor(public navCtrl: NavController, public navParams: NavParams, public viewCtrl: ViewController, private data: DataProvider) {
        this.getData(1);
        this.tempSelected = [];
    }
    getData(page = 1) {
        this.arrayWorkPlace = [];
        this.data.getWorkPlace(page).subscribe(results => {
            var tempArray = this.navParams.get('workplace');
            if (!this.tempSelected.length) {
                if (tempArray) {
                    tempArray.forEach(element => {
                        this.tempSelected.push({ workplace: element.workplace, old: true, selected: true });
                    });
                }
            }
            this.arrayWorkPlace = results["content"];
            this.totalItems = results["totalElements"] || 0;
            this.pager = this.data.getPager(this.totalItems, page);
            var self = this;
            this.arrayWorkPlace.forEach(function (element) {
                element.selected = self.checkSelected(element) ? true : false;
                element.old = element.selected ? true : false;

            });
        }, err => {

        });
    }
    public checkSelected(item) {
        for (let i = 0; i < this.tempSelected.length; i++) {
            const t = this.tempSelected[i];
            if (t.workplace == item.workplace) {
                return true;
            }
        }
        return false;
    }
    checkboxChange(itemSelected) {
        if (itemSelected.selected) {
            this.tempSelected.push(itemSelected);
        } else {
            var tempArray = _.cloneDeep(this.tempSelected);
            this.tempSelected = _.reject(tempArray, function (o) { return o.workplace == itemSelected.workplace; });
        }
    }
    closeModal() {
        this.navCtrl.pop();
        //this.dismiss();        
    }
    ionViewDidLoad() {
        console.log('ionViewDidLoad ModalSelectMrnPage');
    }
    dismiss(data_: Array<any>) {
        this.viewCtrl.dismiss(data_);
    }
    pushData() {
        
        this.dismiss(this.tempSelected);
    }
    add() {
        this.navCtrl.pop().then(() => {
            console.log("selected mrn");
        })
    }
    changePage(event) {
        this.getData(event.pageIndex);
    }

}
