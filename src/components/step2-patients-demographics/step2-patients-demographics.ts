import { Component, Input } from '@angular/core';
import { SearchModel } from '../../providers/model/searchModel';
import { ModalSelectMrnPage } from '../../pages/modal-select-mrn/modal-select-mrn';
import { ModalController } from 'ionic-angular';
import { ModalSelectPatientNamePage } from '../../pages/modal-select-patient-name/modal-select-patient-name';
import { ModalSelectReqLocationPage } from '../../pages/modal-select-req-location/modal-select-req-location';
import * as _ from 'lodash';
@Component({
  selector: 'step2-patients-demographics',
  templateUrl: 'step2-patients-demographics.html'
})
export class Step2PatientsDemographicsComponent {
  @Input() searchModel: SearchModel;
  // public mrnSelectedArray: Array<any>;
  public patientNameSelectedArray: Array<any>;
  public locationSelectedArray: Array<any>;
  constructor(
    private mc: ModalController,
  ) {
    this.searchModel = new SearchModel();
  }
  showMrn() {
    let currentDistinct = this.searchModel.mrn.split('; ') || [];
    this.searchModel.mrnSelectedArray = [];
    currentDistinct.forEach(item => {
      if (item != "") {
        this.searchModel.mrnSelectedArray.push({ id: item });
      }
    });
    let modal = this.mc.create(ModalSelectMrnPage, { mrn: _.cloneDeep(this.searchModel.mrnSelectedArray) }, { enableBackdropDismiss: false, cssClass: 'modal-mrn' });
    modal.onDidDismiss(data => {
      if(data){
        let kipItems = _.filter(data, (v) => {
          return v.selected == true;
        }) || [];
        if (kipItems.length>0) {
          
          let arrAdd = _.map(kipItems, 'id') || [];//arr1
          this.searchModel.mrn = arrAdd.join('; ');
        }else{
          this.searchModel.mrn = "";
        }
      }
    });
    modal.present();
  }
  showPatientNames() {
    
    let currentDistinct = this.searchModel.patientName.split('; ') || [];
    this.searchModel.patientNameSelectedArray = [];
    currentDistinct.forEach(item => {
      if (item != "") {
        this.searchModel.patientNameSelectedArray.push({ id: item });
      }
    });
    let modal = this.mc.create(ModalSelectPatientNamePage, { patientName: _.cloneDeep(this.searchModel.patientNameSelectedArray) }, { enableBackdropDismiss: false, cssClass: 'modal-mrn' });
    modal.onDidDismiss(data => {
      if (data) {
        let kipItems = _.filter(data, (v) => {
          return v.selected == true;
        }) || [];
        if (kipItems.length>0) {
          let arrAdd = _.map(kipItems, 'id') || [];//arr1
          this.searchModel.patientName = arrAdd.join('; ');
        }else{
          this.searchModel.patientName = "";
        }
      }
    });
    modal.present();
  }
  showLocation() {
    
    let currentDistinct = this.searchModel.reqLocation.split('; ') || [];
    this.searchModel.locationSelectedArray = [];
    currentDistinct.forEach(item => {
      if (item != "") {
        this.searchModel.locationSelectedArray.push({ location: item });
      }
    });
    let modal = this.mc.create(ModalSelectReqLocationPage, { location: _.cloneDeep(this.searchModel.locationSelectedArray) }, { enableBackdropDismiss: false, cssClass: 'modal-mrn' });
    modal.onDidDismiss(data => {
      if (data) {
        let kipItems = _.filter(data, (v) => {
          return v.selected == true;
        }) || [];
        if (kipItems.length>0) {
          let arrAdd = _.map(kipItems, 'location') || [];//arr1
          this.searchModel.reqLocation = arrAdd.join('; ');
        }else{
          this.searchModel.reqLocation = "";
        }
      }
    });
    modal.present();
  }

  deleteMRN(index) {
    this.searchModel.mrnSelectedArray.splice(index, 1);
    var tempArray = _.map(this.searchModel.mrnSelectedArray, 'id');
    this.searchModel.mrn = tempArray.filter(function (val) { return val; }).join('; ');
  }
  deletePatientName(index) {
    this.searchModel.patientNameSelectedArray.splice(index, 1);
    var tempArray = _.map(this.searchModel.patientNameSelectedArray, 'name');
    this.searchModel.patientName = tempArray.filter(function (val) { return val; }).join('; ');
  }
  deleteLocation(index) {
    this.searchModel.locationSelectedArray.splice(index, 1);
    var tempArray = _.map(this.searchModel.locationSelectedArray, 'location');
    this.searchModel.reqLocation = tempArray.filter(function (val) { return val; }).join('; ');
  }

}
