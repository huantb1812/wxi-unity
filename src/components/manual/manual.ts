import { Component , Input , Output } from '@angular/core';

/**
 * Generated class for the ManualComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'manual',
  templateUrl: 'manual.html'
})
export class ManualComponent {

  @Input() listOrderResultsManual: any;

  constructor() {
  }
  
  public testcodes: Array<any> = [
    { id: 1, name: 'HGB', testcode: 'HGB', result: '', comment: 100, run1: '', prevres: '100', prevcomment: '' },
    { id: 2, name: 'HCT', testcode: 'HCT', result: '', comment: 100, run1: '', prevres: '100', prevcomment: '' },
    { id: 3, name: 'PLT', testcode: 'PLT', result: '', comment: 100, run1: '', prevres: '100', prevcomment: '' },
  ];


  ngOnInit() {
  }
  
  public changeSelectionTestCode() {

  }


}
