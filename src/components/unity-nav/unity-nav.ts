import { Component, Input } from '@angular/core';
import { AuthProvider } from '../../providers/auth/auth';
import { NavController } from 'ionic-angular';

/**
 * Generated class for the UnityNavComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'unity-nav',
  templateUrl: 'unity-nav.html'
})
export class UnityNavComponent {

  isAdmin:boolean = false;
  isDebug:boolean = false;
  impersonatedUser:any;
  user:any;
  username:string;
  searchToggled:boolean = false;
  @Input('showLogo')showLogo:boolean = true;

  constructor(private auth:AuthProvider, private navCtrl:NavController) {
    
    console.log('Hello UnityNavComponent Component');
    this.user = this.auth._user;
    if(this.user){
      this.username = this.user.FullName;
    }
  }
  search(query:string){
    console.info('search:', query);
    this.navCtrl.push('results', {query:{id:query}});
  }
  searchCancelled(){
    this.searchToggled = false;
  }

}
