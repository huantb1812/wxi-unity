import { NgModule } from '@angular/core';
import { IonicModule } from 'ionic-angular';
import { UnityNavComponent } from './unity-nav/unity-nav';
import { UserSwitcherComponent } from './user-switcher/user-switcher';
import { HeroWelcomeComponent } from './hero-welcome/hero-welcome';
import { PageTopperComponent } from './page-topper/page-topper';
import { TallyBoxComponent } from './tally-box/tally-box';
import { UnityFooterComponent } from './unity-footer/unity-footer';
import { NavHorizontalComponent } from './nav-horizontal/nav-horizontal';
import { Step1SetGlobalPropertiesComponent } from './step1-set-global-properties/step1-set-global-properties';
import { StepTabComponent } from './step-tab/step-tab';
import { Step2PatientsDemographicsComponent } from './step2-patients-demographics/step2-patients-demographics';
import { Step3TestLevelComponent } from './step3-test-level/step3-test-level';
import { Step4SearchResultComponent } from './step4-search-result/step4-search-result';
import { OpAlertsComponent } from './op-alerts/op-alerts';
import { InstrustmentComponent } from './instrustment/instrustment';
import { PatientDemoGraphicsComponent } from './patient-demo-graphics/patient-demo-graphics';
import { LevelSelectionComponent } from './level-selection/level-selection';
import { TestLevelSelectionComponent } from './test-level-selection/test-level-selection';
import { ActionLevelSelectionComponent } from './action-level-selection/action-level-selection';
import { PaginationComponent } from './pagination/pagination';
import { ManualComponent } from './manual/manual';

import {
    MatAutocompleteModule,
    MatButtonModule,
    MatCardModule,
    MatCheckboxModule,
    MatChipsModule,
    MatDatepickerModule,
    MatDialogModule,
    MatDividerModule,
    MatExpansionModule,
    MatGridListModule,
    MatIconModule,
    MatInputModule,
    MatListModule,
    MatMenuModule,
    MatNativeDateModule,
    MatPaginatorModule,
    MatProgressBarModule,
    MatProgressSpinnerModule,
    MatRadioModule,
    MatRippleModule,
    MatSelectModule,
    MatSidenavModule,
    MatSliderModule,
    MatSlideToggleModule,
    MatSnackBarModule,
    MatSortModule,
    MatStepperModule,
    MatTableModule,
    MatTabsModule,
    MatToolbarModule,
    MatTooltipModule,
} from '@angular/material';
@NgModule({
    declarations: [UnityNavComponent,
        UserSwitcherComponent,
        HeroWelcomeComponent,
        PageTopperComponent,
        HeroWelcomeComponent,
        TallyBoxComponent,
        UnityFooterComponent,
        NavHorizontalComponent,
        Step1SetGlobalPropertiesComponent,
        StepTabComponent,
        Step2PatientsDemographicsComponent,
        Step3TestLevelComponent,
        Step4SearchResultComponent,
        OpAlertsComponent,
        InstrustmentComponent,
        PatientDemoGraphicsComponent,
        LevelSelectionComponent,
        TestLevelSelectionComponent,
        ActionLevelSelectionComponent,
        PaginationComponent,
        ManualComponent
    ],
    imports: [IonicModule,
        MatAutocompleteModule,
        MatButtonModule,
        MatCardModule,
        MatCheckboxModule,
        MatChipsModule,
        MatDatepickerModule,
        MatDialogModule,
        MatDividerModule,
        MatExpansionModule,
        MatGridListModule,
        MatIconModule,
        MatInputModule,
        MatListModule,
        MatMenuModule,
        MatNativeDateModule,
        MatPaginatorModule,
        MatProgressBarModule,
        MatProgressSpinnerModule,
        MatRadioModule,
        MatRippleModule,
        MatSelectModule,
        MatSidenavModule,
        MatSliderModule,
        MatSlideToggleModule,
        MatSnackBarModule,
        MatSortModule,
        MatStepperModule,
        MatTableModule,
        MatTabsModule,
        MatToolbarModule,
        MatTooltipModule,],
    exports: [UnityNavComponent,
        UserSwitcherComponent,
        HeroWelcomeComponent,
        PageTopperComponent,
        HeroWelcomeComponent,
        TallyBoxComponent,
        UnityFooterComponent,
        NavHorizontalComponent,
        Step1SetGlobalPropertiesComponent,
        StepTabComponent,
        Step2PatientsDemographicsComponent,
        Step3TestLevelComponent,
        Step4SearchResultComponent,
        OpAlertsComponent,
        InstrustmentComponent,
        PatientDemoGraphicsComponent,
        LevelSelectionComponent,
        TestLevelSelectionComponent,
        ActionLevelSelectionComponent,
        PaginationComponent,
        ManualComponent
    ]
})
export class ComponentsModule { }
