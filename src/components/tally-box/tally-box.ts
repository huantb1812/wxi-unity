import { Component, Input } from '@angular/core';

/**
 * Generated class for the TallyBoxComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'tally-box',
  templateUrl: 'tally-box.html'
})
export class TallyBoxComponent {
  @Input() tally:number;
  @Input() label:string;


  constructor() {
    console.log('Hello TallyBoxComponent Component');
  }

}
