import { Component, OnInit } from '@angular/core';

import * as moment from 'moment';
/**
 * Generated class for the UnityFooterComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'unity-footer',
  templateUrl: 'unity-footer.html'
})
export class UnityFooterComponent implements OnInit  {
  copyright:string;
  lastUpdate:string;
  constructor() {
    console.log('Hello UnityFooterComponent Component');

  }
  ngOnInit(){
    this.copyright = '© '+ new Date().getFullYear() + ' Sysmex America, Inc. All Rights Reserved';
    //this.lastUpdate = 'Last Updated: ' + moment().subtract(5, 'minutes').fromNow();
  }

}
