import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Events, AlertController, PopoverController } from "ionic-angular";
import { UsersPage } from '../../pages/users/users';
import { AuthProvider } from '../../providers/auth/auth';
/**
 * Generated class for the UserSwitcherComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'user-switcher',
  templateUrl: 'user-switcher.html'
})
export class UserSwitcherComponent {
  @Input() available: boolean = false;
  @Input() users: any[];
  @Input() currentUsername:string;
  @Output()itemClick:EventEmitter<any> = new EventEmitter<any>();
  user:any;
  constructor(
    private auth:AuthProvider,
    private events: Events,
    private ac: AlertController,
    private pc: PopoverController
  ) {
    console.log('Hello UserSwitcherComponent Component');
    this.events.subscribe('USER', this.update);
    this.user = this.auth._user;

  }

  launch(evt:any){
    console.info('launch', evt);
    //TODO: Implement Users List if needed for POC
    let pop = this.pc.create(UsersPage, {}, {cssClass:'users'});
    pop.present({
      ev:evt
    }).then(()=>{
      console.info('presented ok');
    }, (reason)=>{
      console.warn('present returned error', reason);
    })
  }

  update = (user)=>{
    console.info('UserSwitcherComponent: user', user);

    if(user && user.FullName) {
      this.currentUsername = user.FullName;
    }
    this.user = user;
  }

  userChanged(username){
    console.info('AdminUserComponent:changed', username);
    this.itemClick.emit(username);
  }


  click(item) {
    this.itemClick.emit(item);
  }

}
