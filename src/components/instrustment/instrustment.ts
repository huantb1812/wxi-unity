import { Component, Input, Output, OnChanges } from '@angular/core';
import { DataProvider } from '../../providers/data/data';
import { TypeSort } from '../../providers/model/typeSort';
/**
 * Generated class for the InstrustmentComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'instrustment',
  templateUrl: 'instrustment.html'
})
export class InstrustmentComponent implements OnChanges {
  @Input() listOrderResultsInstrusment: any;
  @Input() conditionSearch: any;
  public typeSortIntrusment: string;
  constructor(private data: DataProvider) {
    //this.typeSort = 'asc';
  }
  public testcodes: Array<any> = [
    { id: 1, name: 'HGB', testcode: 'HGB', result: '', comment: 100, run1: '', prevres: '100', prevcomment: '' },
    { id: 2, name: 'HCT', testcode: 'HCT', result: '', comment: 100, run1: '', prevres: '100', prevcomment: '' },
    { id: 3, name: 'PLT', testcode: 'PLT', result: '', comment: 100, run1: '', prevres: '100', prevcomment: '' },
  ];


  ngOnInit() {
  }
  ngOnChanges(){
    //console.log(this.conditionSearch);
    if(this.conditionSearch.sort){
      this.typeSortIntrusment = this.conditionSearch.sort;
    }else{
      this.typeSortIntrusment = 'asc';
    }
  }

  sort() {
    this.typeSortIntrusment = this.typeSortIntrusment == 'desc' ? 'asc' : 'desc';
    this.data.loadOrderResults(this.conditionSearch.lPeriod, this.conditionSearch.lId, this.conditionSearch.orderNumber, this.typeSortIntrusment).subscribe(res => {
      let dataListOrderResuls = res.content;
      this.listOrderResultsInstrusment = [];
      if (dataListOrderResuls.length) {
        dataListOrderResuls.forEach(element => {
          this.listOrderResultsInstrusment.push(element);
        });
      }
    },error =>{

    });
  }

  public changeSelectionTestCode() {

  }

}
