import { IonicPage, NavController, NavParams, ModalController, ViewController } from 'ionic-angular';
import { TestCodeMultiComponent } from "../../pages/modal-select-testcode/modal-select-testcode-multi";
import { ModalSelectOPAlertPage } from '../../pages/model-select-opalert/modal-select-opalert';
import { DataProvider } from "../../providers/data/data";
import * as _ from 'lodash';
import { Component, Input, OnInit } from '@angular/core';
import { CreateRuleModel } from '../../providers/model/createRuleModel';

/**
 * Generated class for the ActionLevelSelectionComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'action-level-selection',
  templateUrl: 'action-level-selection.html'
})
export class ActionLevelSelectionComponent {
  @Input() createModel: CreateRuleModel;
  reRunTestListArray: Array<any>;
  reviewTestListArray: Array<any>;
  reflexTestListArray: Array<any>;
  deleteTestListArray: Array<any>;
  opPriorityArray: Array<any>;
  constructor(public navCtrl: NavController, public navParams: NavParams, private mc: ModalController, private data: DataProvider) {
    this.createModel = new CreateRuleModel();
    this.opPriorityArray = [];
    this.pushDataPriorityArray();
  }
  fillDataTestList() {
    if (this.createModel.rerunTestList) {
      var tempArray = this.createModel.rerunTestList.split(",");
      var self = this;
      tempArray.forEach(function (element) {
        var arr = { testcode: element };
        self.reRunTestListArray.push(arr);
      });
    }
    if (this.createModel.rerunTestList) {
      var tempArray = this.createModel.rerunTestList.split(",");
      var self = this;
      tempArray.forEach(function (element) {
        var arr = { testcode: element };
        self.reRunTestListArray.push(arr);
      });
    }
  }
  ionViewDidLoad() {
    console.log("HHH");
  }
  reRunClick(event) {
    if (!event.checked) {
      this.createModel.reRunTestListArray = [];
      this.createModel.rerunTestList = "";
      this.createModel.rerunSelection = 0;
    }
  }

  reviewClick(event) {
    if (!event.checked) {
      this.createModel.reviewTestListArray = [];
      this.createModel.reviewTestList = "";
      this.createModel.reviewSelection = 0;
    }
  }
  reRunSlectionChange() {
    console.log(this.createModel.rerunSelection);
    if (this.createModel.rerunSelection != 3) {
      this.createModel.reRunTestListArray = [];
      this.createModel.rerunTestList = "";
    }
  }

  reviewSlectionChange() {
    console.log(this.createModel.reviewSelection);
    if (this.createModel.reviewSelection != 3) {
      this.createModel.reviewTestListArray = [];
      this.createModel.reviewTestList = "";
    }
  }

  pushDataPriorityArray() {
    var first = "a", last = "z";
    for (var i = first.charCodeAt(0); i <= last.charCodeAt(0); i++) {
      this.opPriorityArray.push(eval("String.fromCharCode(" + i + ")").toUpperCase());
    }
  }
  showReRunTestCode() {

    if(this.createModel.rerunTestList==null)this.createModel.rerunTestList='';

    let currentDistinct = this.createModel.rerunTestList.split(', ') || [];
    this.createModel.reRunTestListArray = [];
    currentDistinct.forEach(item => {
      if (item != "") {
        this.createModel.reRunTestListArray.push({ testCode: item });
      }
    });

    let modal = this.mc.create(TestCodeMultiComponent, { testcode: _.cloneDeep(this.createModel.reRunTestListArray) }, { enableBackdropDismiss: false, cssClass: 'modal-mrn' });
    modal.onDidDismiss(data => {
  
      if (data) {
        // this.createModel.reRunTestListArray = data;
        // var tempArray = _.map(data, 'testCode');
        // this.createModel.rerunTestList = tempArray.filter(function (val) { return val; }).join(',');
        let kipItems = _.filter(data, (v) => {
          return v.selected == true;
        }) || [];
        if (kipItems.length>0) {
          let arrAdd = _.map(kipItems, 'testCode') || [];//arr1
          this.createModel.rerunTestList = arrAdd.join(', ');
        }else{
          this.createModel.rerunTestList = "";
        }
      }
    });
    modal.present();
  }
  showReviewTestCode() {
    if(this.createModel.reviewTestList==null)this.createModel.reviewTestList='';

    let currentDistinct = this.createModel.reviewTestList.split(', ') || [];
    this.createModel.reviewTestListArray = [];
    currentDistinct.forEach(item => {
      if (item != "") {
        this.createModel.reviewTestListArray.push({ testCode: item });
      }
    });
    let modal = this.mc.create(TestCodeMultiComponent, { testcode: _.cloneDeep(this.createModel.reviewTestListArray) }, { enableBackdropDismiss: false, cssClass: 'modal-mrn' });
    modal.onDidDismiss(data => {
      if (data) {
        // this.createModel.reviewTestListArray = data;
        // var tempArray = _.map(data, 'testCode');
        // this.createModel.reviewTestList = tempArray.filter(function (val) { return val; }).join(',');
        let kipItems = _.filter(data, (v) => {
          return v.selected == true;
        }) || [];
        if (kipItems.length>0) {
          let arrAdd = _.map(kipItems, 'testCode') || [];//arr1
          this.createModel.reviewTestList = arrAdd.join(', ');
        }else{
          this.createModel.reviewTestList =  "";
        }
      }
    });
    modal.present();
  }
  showReflexTestCode() {
    if(this.createModel.reflexTestList==null)this.createModel.reflexTestList='';

    let currentDistinct = this.createModel.reflexTestList.split(', ') || [];
    this.createModel.reflexTestListArray = [];
    currentDistinct.forEach(item => {
      if (item != "") {
        this.createModel.reflexTestListArray.push({ testCode: item });
      }
    });
    let modal = this.mc.create(TestCodeMultiComponent, { testcode: _.cloneDeep(this.createModel.reflexTestListArray) }, { enableBackdropDismiss: false, cssClass: 'modal-mrn' });
    modal.onDidDismiss(data => {
      if (data) {
        // this.createModel.reflexTestListArray = data;
        // var tempArray = _.map(data, 'testCode');
        // this.createModel.reflexTestList = tempArray.filter(function (val) { return val; }).join(',');
        let kipItems = _.filter(data, (v) => {
          return v.selected == true;
        }) || [];
        if (kipItems.length>0) {
          let arrAdd = _.map(kipItems, 'testCode') || [];//arr1
          this.createModel.reflexTestList = arrAdd.join(', ');
        }else{
          this.createModel.reflexTestList = "";
        }
      }
    });
    modal.present();
  }
  showDeleteTestCode() {
    if(this.createModel.deleteTestList==null)this.createModel.deleteTestList='';
    let currentDistinct = this.createModel.deleteTestList.split(', ') || [];
    this.createModel.deleteTestListArray = [];
    currentDistinct.forEach(item => {
      if (item != "") {
        this.createModel.deleteTestListArray.push({ testCode: item });
      }
    });
    let modal = this.mc.create(TestCodeMultiComponent, { testcode: _.cloneDeep(this.createModel.deleteTestListArray) }, { enableBackdropDismiss: false, cssClass: 'modal-mrn' });
    modal.onDidDismiss(data => {
      if (data) {
        // this.createModel.deleteTestListArray = data;
        // var tempArray = _.map(data, 'testCode');
        // this.createModel.deleteTestList = tempArray.filter(function (val) { return val; }).join(',');
        let kipItems = _.filter(data, (v) => {
          return v.selected == true;
        }) || [];
        if (kipItems.length>0) {
          let arrAdd = _.map(kipItems, 'testCode') || [];//arr1
          this.createModel.deleteTestList = arrAdd.join(', ');
        }else{
          this.createModel.deleteTestList = "";
        }
      }
    });
    modal.present();
  }
  deleteRerunList(index) {
    this.createModel.reRunTestListArray.splice(index, 1);
    var tempArray = _.map(this.createModel.reRunTestListArray, 'testCode');
    this.createModel.rerunTestList = tempArray.filter(function (val) { return val; }).join(', ');
  }
  deleteReviewList(index) {
    this.createModel.reviewTestListArray.splice(index, 1);
    var tempArray = _.map(this.createModel.reviewTestListArray, 'testCode');
    this.createModel.reviewTestList = tempArray.filter(function (val) { return val; }).join(', ');
  }
  deleteReflexList(index) {
    this.createModel.reflexTestListArray.splice(index, 1);
    var tempArray = _.map(this.createModel.reflexTestListArray, 'testCode');
    this.createModel.reflexTestList = tempArray.filter(function (val) { return val; }).join(', ');
  }
  deleteDeleteList(index) {
    this.createModel.deleteTestListArray.splice(index, 1);
    var tempArray = _.map(this.createModel.deleteTestListArray, 'testCode');
    this.createModel.deleteTestList = tempArray.filter(function (val) { return val; }).join(', ');
  }
  ShowOPAlert() {
    let modal = this.mc.create(ModalSelectOPAlertPage, { testcode: "" }, { enableBackdropDismiss: false, cssClass: 'modal-mrn' });
    modal.onDidDismiss(data => {
      if (data) {
        this.createModel.oPAlertId = data.opAlertCode;
        this.createModel.oPAlertDescription = data.oatext;
        this.createModel.oPAlertPriority = data.priority;
      }
    });
    modal.present();
  }
}
