import { IonicPage, NavController, NavParams, ModalController, ViewController, AlertController  } from 'ionic-angular';
import { TestCodeMultiComponent } from "../../pages/modal-select-testcode/modal-select-testcode-multi";
import { TestCodeSingleComponent } from '../../pages/modal-select-testcode/modal-select-testcode-single';
import { ModalSelectSingleWorkPlacePage } from '../../pages/modal-select-workplace/modal-select-workplace-single';
import { DataProvider } from "../../providers/data/data";
import * as _ from 'lodash';
import { Component, Input, OnInit } from '@angular/core';
import { CreateRuleModel } from '../../providers/model/createRuleModel';

/**
 * Generated class for the TestLevelSelectionComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'test-level-selection',
  templateUrl: 'test-level-selection.html'
})
export class TestLevelSelectionComponent {
  @Input() createModel: CreateRuleModel;
  arrayTestCode: Array<any>;
  arrayDeltaType = [{ id: 0, name: "Ignore" },
  { id: 1, name: "Previous order" },
  { id: 2, name: "Previous Run" },
  { id: 3, name: "No Previous" }];
 
  arrayCheckType = [{ id: 0, name: "Present" },
  { id: 1, name: "Outside of" },
  { id: 2, name: "Inside of" },
  { id: 3, name: "Coded" },
  { id: 4, name: "Not present" }];

  constructor(public navCtrl: NavController, public navParams: NavParams, private mc: ModalController, private data: DataProvider, private alertCtrl: AlertController) {
    this.createModel = new CreateRuleModel()
    this.createModel.ruleElements = new Array<any>();
    // this.arrayTestLevelAdding = new Array<any>();
  }
  disabledLow(item) {
    if (item.checkType != 1 && item.checkType !=  2) {
      return true;
    }
    return false;
  }
  disabledHight(item) {
    if (item.checkType != 1 && item.checkType !=  2) {
      return true;
    }
    return false;
  }
  showTestCodeTestLevel(index) {
    var tempValue = this.createModel.ruleElements[index].testCode;
    let modal = this.mc.create(TestCodeSingleComponent, { testcode: _.cloneDeep(tempValue) }, { enableBackdropDismiss: false, cssClass: 'modal-mrn' });
    modal.onDidDismiss(data => {
      if (data) {
        if(data.selected){
          var exist = this.checkExistTestCode(data["testCode"]);
          if(exist){
            let alertDelete = this.alertCtrl.create({
              title: '',
              message: 'Test Code has existed',
              buttons: ['OK']
            });
            alertDelete.present();
          }else{
            this.createModel.ruleElements[index].testCode = data["testCode"];
          }
          
        }else{
          this.createModel.ruleElements[index].testCode = "";
        }        
      }
    });
    modal.present();
  }

  showInstr(index){
    var tempValue = this.createModel.ruleElements[index].instrument;
    let modal = this.mc.create(ModalSelectSingleWorkPlacePage, { instr: _.cloneDeep(tempValue) }, { enableBackdropDismiss: false, cssClass: 'modal-mrn' });
    modal.onDidDismiss(data => {
      if (data) {
        if(data.selected){
          this.createModel.ruleElements[index].instrument = data["workplace"];
        }else{
          this.createModel.ruleElements[index].instrument = "";
        }        
      }
    });
    modal.present();
  }

  DeleteTestLevel(index) {
    var deleteSuccess = false;
    let alert = this.alertCtrl.create({
      title: '',
      message: 'Are you sure you want to delete this record?',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
            //console.log('Cancel clicked');
          }
        },
        {
          text: 'OK',
          handler: () => {
            this.createModel.ruleElements.splice(index, 1);
            deleteSuccess = true;
            //console.log('Buy clicked');
            let alertDelete = this.alertCtrl.create({
              title: '',
              message: 'Record was deleted successful.',
              buttons: ['OK']
            });
            alertDelete.present();
          }
        }
      ]
    });
    alert.present();

    
  }

  AddRow() {
    var checkTestCode = false;
    if (this.createModel.ruleElements.length > 0) {
      var check = this.createModel.ruleElements[this.createModel.ruleElements.length - 1].testCode;
      if (check) {
        checkTestCode = true;
      } else {
        checkTestCode = false;
      }
    } else {
      checkTestCode = true;
    }

    if (checkTestCode) {
      var tempArr = {
        testCode: "",
        instrument: "",
        checkType: 0,
        lowLimit: "",
        highLimit: "",
        flag: "",
        deltaType: 0
      }
      this.createModel.ruleElements.push(tempArr);
    }

  }

  checkExistTestCode(testCode){
    if(this.createModel.ruleElements){
      var exist = _.findIndex(this.createModel.ruleElements, ['testCode', testCode]);
      if(exist == -1){
        return false;
      }else{
        return true;
      }
    }
    return false;
  }
}
