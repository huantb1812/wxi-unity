import { Component } from '@angular/core';

/**
 * Generated class for the PageTopperComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'page-topper',
  templateUrl: 'page-topper.html'
})
export class PageTopperComponent {

  text: string;

  constructor() {
    console.log('Hello PageTopperComponent Component');
    this.text = 'Hello World';
  }

}
