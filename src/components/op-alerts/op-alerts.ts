import { Component , Input , Output } from '@angular/core';

/**
 * Generated class for the OpAlertsComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'op-alerts',
  templateUrl: 'op-alerts.html'
})
export class OpAlertsComponent {
  @Input() listOpAlert: any;

  constructor() {
  }

  ngOnInit() {
  }

}
