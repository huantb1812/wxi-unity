import { Component, Input, Output } from '@angular/core';

/**
 * Generated class for the PatientDemoGraphicsComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'patient-demo-graphics',
  templateUrl: 'patient-demo-graphics.html'
})
export class PatientDemoGraphicsComponent {
  @Input() patientDemoGraphics: any;

  constructor() {
    /*
        "pk": {
      "lPeriod": "LPERIOD2",
      "orderNumber": "ORDER2",
      "lid": "LID2"
    },
    "sampleId": "ID2",
    "discipline": null,
    "requestingLocation": null,
    "requestingPhysician": null,
    "physicianName": null,
    "physicianPhone": null,
    "careUnit": null,
    "roomOrBed": null,
    "mrn": null,
    "patientName": null,
    "patientBirthDate": null,
    "patientSex": null,
    "patientType": null,
    "receiptDateTime": null,
    "sampleDateTime": null,
    "stat": null,
    "site": null,
    "originType": null,
    "resultStatus": null,
    "diagnosis1": null,
    "diagnosis2": null,
    "statMode": null,
    "critical": null,
    "age": null,
    "reportDate": null,
    "dispatchCodes": null,
    "dateModified": null,
    "modifyingUser": null,
    "dateCreated": null,
    "creatingUser": null,
    "workplace": null,
    "refGroup": null,
    "extMm": null,
    "pregnancyFlag": null,
    "externalReference": null,
    "fastingSt": null,
    "trtNb": null
     */
    // this.patientDemoGraphics = {
    //   sample_Id: 'ID 01',
    //   collection_dt: -1,
    //   req_location: 'req_location',

    //   mrn: 'SYS0001',
    //   receipt_dt: -1,
    //   rep_phys: 'rep_phys',

    //   name: 'combobox',
    //   sample_loc: -1,
    //   req_name: 'req_name',



    //   dob:'18/12/1984',
    //   age: 34,
    //   sex: 1,
    //   diagnosis_1: 'diagnosis_1',
    //   phone: '0972170432',

    //   diagnosis_2: 'diagnosis_2',
    //   care_unit: 'care_unit',
    // }
  }

}
