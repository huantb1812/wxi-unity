import { Component, EventEmitter, Output, Input } from '@angular/core';
import { AuthProvider } from '../../providers/auth/auth';
import { Events } from 'ionic-angular';

/**
 * Generated class for the HeroWelcomeComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'hero-welcome',
  templateUrl: 'hero-welcome.html'
})
export class HeroWelcomeComponent {
  query:string;
  @Input() salutation:string;
  @Input() heroStatus:string;
  @Input() prompt:string = 'What can we help you find?';
  @Output()onSearch:EventEmitter<any> = new EventEmitter<any>();


  constructor(private auth:AuthProvider, private events:Events) {
    console.log('Hello HeroWelcomeComponent Component');
    this.updateSalutations();
    this.events.subscribe('USER', this.updateSalutations);
  }

  updateSalutations = ()=>{
    this.auth.user().subscribe(user=>{
      console.info('updateSalutations', user);
      let name = (user && user.FirstName)?', '+user.FirstName:'';
      let now = new Date().getHours();
      if(now < 13){
        this.salutation = 'Good Morning'+name+'!';
      } else  if(now < 18) {
        this.salutation = 'Good Afternoon'+name+'!';
      } else {
        this.salutation = 'Good Evening'+name+'!';
      }
    })
  }

  search(){
    this.onSearch.emit(this.query);
  }

}
