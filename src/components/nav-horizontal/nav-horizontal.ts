import { Component } from '@angular/core';

/**
 * Generated class for the NavHorizontalComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'nav-horizontal',
  templateUrl: 'nav-horizontal.html'
})
export class NavHorizontalComponent {

  text: string;
  public user: any;
  constructor() {
    console.log('Hello NavHorizontalComponent Component');
    this.text = 'Hello World';
    this.user = {
      UserName: "Administrator"
    }
  }
  logout() { }

}
