import { Component, Output, EventEmitter } from '@angular/core';


@Component({
  selector: 'step-tab',
  templateUrl: 'step-tab.html'
})
export class StepTabComponent {
  @Output() onSelectStep: EventEmitter<any>;
  constructor() {
    this.onSelectStep = new EventEmitter();
  }
  selectStep(step) {
    this.onSelectStep.emit(step);
  }
}
