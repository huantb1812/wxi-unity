import { Component, Input } from '@angular/core';
import { CreateRuleModel } from '../../providers/model/createRuleModel';


@Component({
  selector: 'level-selection',
  templateUrl: 'level-selection.html'
})
export class LevelSelectionComponent {
  @Input() createModel: CreateRuleModel;
  constructor() {
    this.createModel = new CreateRuleModel();
  }

}
