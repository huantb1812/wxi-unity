import { Component, EventEmitter, Input, Output, OnChanges } from '@angular/core';
import { MatPaginator, MatSort, MatTableDataSource, MatButtonToggle, MatButton } from '@angular/material';
import { Storage } from '@ionic/storage';
import { Events, NavParams, NavController } from 'ionic-angular';
import { AuthProvider } from '../../providers/auth/auth';
import { DataProvider } from '../../providers/data/data';
import { TypeSort } from '../../providers/model/typeSort';
/**
 * Generated class for the Step4SearchResultComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */

enum parent {
  rulefollowdifinition = 'rulefollowdifinition'
}
@Component({
  selector: 'step4-search-result',
  templateUrl: 'step4-search-result.html'
})
export class Step4SearchResultComponent implements OnChanges {
  //demo
  dataSource = new MatTableDataSource();
  rulefollowdifinitionpage:string=parent.rulefollowdifinition;
  @Input() parent: any;
  //demo
  @Input() loading: boolean;
  @Input() resultsAllData: any;
  @Input() pager: any;
  @Input() searchResult: Array<any>;
  @Output() onUpdateCart: EventEmitter<any>;
  @Output() onChangePage: EventEmitter<any>;

  @Input() arrSort:any;

  public cart: Array<any>;
  public curentDetail: any;
  public user: any;

  public typeSort: string;
  public fieldSort: string;
  
  // sampleDetailsDataSource = new MatTableDataSource();


  constructor(private navCtrl: NavController, private auth: AuthProvider, private storage: Storage, public navParams: NavParams,
    private events: Events,
    private data: DataProvider) {
    this.loading = true;
    this.user = {};
    this.searchResult = [];
    this.cart = [];
    this.onUpdateCart = new EventEmitter();
    this.onChangePage = new EventEmitter();
    this.events.subscribe('USER', this.update);
    this.user = this.auth._user;
    this.user = this.navParams.get('user') || null;
    this.resultsAllData = {
      content: [],
      totalItems: 0
    }
    // if (this.auth._user) {
    //   this.user = this.auth._user;
    // }
    this.typeSort = TypeSort.none;
    this.fieldSort = '';
  }
  ngOnChanges(){
    if(this.arrSort){
        this.typeSort = this.arrSort.typeSort? this.arrSort.typeSort:'';      

        this.fieldSort = this.arrSort.fieldSort? this.arrSort.fieldSort : '';     
    }
  }
  ngOnInit() {


  }
  update = (user) => {
    if (user) {
      this.user = user;
    }
  }
  changeCheckBox(event, item, index) {
    for (let i = 0; i < this.searchResult.length; i++) {
      let item = this.searchResult[i];
      if (i == index) {
        if (item.selected != true) {
          this.curentDetail = item;
          this.showDetail(item);
        }
        else {
          this.curentDetail = null;
        }
      } else {
        item.selected = false;
      }

    }
    if(this.parent == parent.rulefollowdifinition){
      //todo
      this.onUpdateCart.emit(item);
    }
    console.log(this.searchResult);
  }
  /**
   * clearDetailItem
   */
  public clearDetailItem() {
    this.curentDetail = null;
  }
  /**
   * showDetail
   */
  public showDetail(item) {
    if (this.parent != parent.rulefollowdifinition) {
      for (let i = 0; i < this.searchResult.length; i++) {
        const element = this.searchResult[i];
        element.selected = false;
      }
      item.selected = true;
      this.curentDetail = item;
    
      let searchResultTable: HTMLElement = document.querySelector(".table-result") as HTMLElement;
      if (searchResultTable != null) {
        searchResultTable.removeAttribute("col-12");
        searchResultTable.setAttribute("col-8", "");
      }
      let detailItem: HTMLElement = document.querySelector(".detail") as HTMLElement;

      if (detailItem != null) {
        detailItem.classList.remove("collapse");
      }

    }
  }

  AddRuleSelected(item){
    if(this.parent == parent.rulefollowdifinition){
      //todo
      this.onUpdateCart.emit(item);
      //console.log(item);
    }
  }
  /**
   * AddToCart
   */
  public AddToCart() {
    this.cart = [];
    let countSelected = 0;
    for (let i = 0; i < this.searchResult.length; i++) {
      let item = this.searchResult[i];
      if (item.selected) {
        this.cart.push(item);
        countSelected++;
      }
    }

    if (countSelected > 0) {
      // this.toastr.success('Hello world!', 'Toastr fun!');

      // this.toastr.success(countSelected + ' rules added');
      // setTimeout(() => {
      //   this.onUpdateCart.emit(this.cart);
      // }, 800);

    }
  }
  newrule(item) {
    this.navCtrl.push('createrule', { rule: item, isUpdate: false });
  }
  editrule(item) {
    this.navCtrl.push('createrule', { rule: item, isUpdate: true });

  }
  changePage(event) {
    if (this.typeSort != TypeSort.none) {
      event.fieldSort = this.fieldSort;
      event.typeSort = this.typeSort;
    }
    this.onChangePage.emit(event);
  }

  sort(field) {
    if(this.pager){
      if (this.fieldSort != field) {
        this.fieldSort = field;
        this.typeSort = TypeSort.asc;
      } else {
        switch (this.typeSort) {
          //case TypeSort.none: this.typeSort = TypeSort.asc; break;
          case TypeSort.asc: this.typeSort = TypeSort.desc; break;
          case TypeSort.desc:  this.typeSort = TypeSort.asc; break;
        }
      }
      console.log(this.fieldSort, this.typeSort);
      //call 
      /*
      pageIndex:2
      pageSize:10
       */
      this.changePage({ pageIndex:this.pager.currentPage, pageSize: 10, fieldSort : this.fieldSort, typeSort: this.typeSort });
    }
    
  }
}
