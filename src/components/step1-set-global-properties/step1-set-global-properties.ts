import { Component, Input } from '@angular/core';
import { SearchModel } from '../../providers/model/searchModel';
import { AuthProvider } from '../../providers/auth/auth';
import { Events } from 'ionic-angular';

@Component({
  selector: 'step1-set-global-properties',
  templateUrl: 'step1-set-global-properties.html'
})
export class Step1SetGlobalPropertiesComponent {
  @Input() searchModel: SearchModel;
  constructor(
  ) {
    this.searchModel = new SearchModel();
  }
  ngOnInit() {
  }
  test() {
  }

 

}
