import { Component, Input } from '@angular/core';
import { SearchModel } from '../../providers/model/searchModel';
import { DataProvider } from "../../providers/data/data";

@Component({
  selector: 'step3-test-level',
  templateUrl: 'step3-test-level.html'
})
export class Step3TestLevelComponent {
  @Input() searchModel: SearchModel;
  public testCodeSelected: Array<any> = [];
  public pager: any;
  public totalItems: any;

  constructor(private data: DataProvider) {
    this.searchModel = new SearchModel();
  }
  public testcodes: Array<any> = [];


  ngOnInit() {
    // this.data.getTestCode().subscribe(res => {
    //   this.testcodes = res["content"] || [];

    // });
    this.getData(1);
  }
  checkSelected(item) {
    for (let i = 0; i < this.testCodeSelected.length; i++) {
      const t = this.testCodeSelected[i];
      if (t.testCode == item.testCode) {
        return true;
      }
    }
    return false;
  }

  getData(page = 1) {
    this.testcodes = [];
    this.data.getTestCode(page).subscribe(results => {
      this.testcodes = results["content"] || [];
      this.totalItems = results["totalElements"] || 0;
      this.pager = this.data.getPager(this.totalItems, page);
      for (let i = 0; i < this.testcodes.length; i++) {
        const item = this.testcodes[i];
        item.selected = this.checkSelected(item) ? true : false;
      }
    }, err => {

    });
  }
  updateTestCodeSelected(item) {
    if (item.selected) {
      this.testCodeSelected.push(item);
    } else {
      for (let i = 0; i < this.testCodeSelected.length; i++) {
        const element = this.testCodeSelected[i];
        if (element.testCode == item.testCode) {
          this.testCodeSelected.splice(i, 1);
          break;
        }
      }
    }
  }
  public changeSelectionTestCode(event) {
    this.updateTestCodeSelected(event);
    this.searchModel.testCode = [];
    for (let i = 0; i < this.testCodeSelected.length; i++) {
      let item = this.testCodeSelected[i];

      if (item.selected == true) {
        this.searchModel.testCode.push(item);
      }
    }
  }
  public clearSelection() {
    this.searchModel.testCode = [];
    this.testCodeSelected=[];
    for (let i = 0; i < this.testcodes.length; i++) {
      this.testcodes[i].selected = false;
    }
  }
  changePage(event) {
    this.getData(event.pageIndex);
  }

}
