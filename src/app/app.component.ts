import { Component } from '@angular/core';
import { Events, Platform, ModalController } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { TabsPage } from "../pages/tabs/tabs";
import { AuthProvider } from '../providers/auth/auth';
import { LoginPage } from '../pages/login/login';
import { RuleFollowDifinitionPage } from '../pages/rule-follow-difinition/rule-follow-difinition';
import { ModalRuleFollowAddPage } from '../pages/modal-rule-follow-add/modal-rule-follow-add';
@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  // rootPage: any = RuleFollowDifinitionPage;
  // rootPage: any = ModalRuleFollowAddPage;
  rootPage: any = TabsPage;

  constructor(private auth: AuthProvider,
    private events: Events,
    private mc: ModalController,
    private platform: Platform,
    private statusBar: StatusBar,
    private splashScreen: SplashScreen) {
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();
      // //dev
      this.checkForUser();
      this.events.subscribe('NEED_LOGIN', this.checkForUser);
    });
  }

  private checkForUser() {
    this.auth.user().subscribe(user => {
      if (!user) {
        let modal = this.mc.create(LoginPage, null, { enableBackdropDismiss: false, cssClass: 'modal-login' }, );

        modal.onDidDismiss(data => {
          console.log('MODAL DATA', data);
        });
        modal.present();
      }
    })
  }
}
