import { NgModule, ErrorHandler } from "@angular/core";
import { BrowserModule } from "@angular/platform-browser";
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { IonicApp, IonicModule, IonicErrorHandler } from "ionic-angular";
import { IonicStorageModule } from "@ionic/storage";
import { MyApp } from "./app.component";

import { MatFormFieldModule } from '@angular/material/form-field';
import {
  // MatButtonToggle,
  MatAutocompleteModule,
  MatButtonModule,
  // MatButtonToggleModule,
  MatCardModule,
  MatCheckboxModule,
  MatChipsModule,
  MatDatepickerModule,
  MatDialogModule,
  MatDividerModule,
  MatExpansionModule,
  MatGridListModule,
  MatIconModule,
  MatInputModule,
  MatListModule,
  MatMenuModule,
  MatNativeDateModule,
  MatPaginatorModule,
  MatProgressBarModule,
  MatProgressSpinnerModule,
  MatRadioModule,
  MatRippleModule,
  MatSelectModule,
  MatSidenavModule,
  MatSliderModule,
  MatSlideToggleModule,
  MatSnackBarModule,
  MatSortModule,
  MatStepperModule,
  MatTableModule,
  MatTabsModule,
  MatToolbarModule,
  MatTooltipModule,
} from '@angular/material';


import { AlertsPage } from "../pages/alerts/alerts";
import { DashboardPage } from "../pages/dashboard/dashboard";
import { LoginPage } from "../pages/login/login";
import { SamplesPage } from "../pages/samples/samples";
import { SettingsPage } from "../pages/settings/settings";
import { TabsPage } from "../pages/tabs/tabs";
import { RuleWizardPage } from "../pages/rule-wizard/rule-wizard";
import { SampleExplorerPage } from "../pages/sample-explorer/sample-explorer";
import { ResultValidationPage } from "../pages/result-validation/result-validation";
import { CreateRulePage } from "../pages/create-rule/create-rule";
// child rule wizard page
import { ModalSelectMrnPage } from "../pages/modal-select-mrn/modal-select-mrn";
import { ModalSelectPatientNamePage } from "../pages/modal-select-patient-name/modal-select-patient-name";
import { ModalSelectReqLocationPage } from "../pages/modal-select-req-location/modal-select-req-location";
import { ModalSelectWorkPlacePage } from "../pages/modal-select-workplace/modal-select-workplace";
import { TestCodeMultiComponent } from "../pages/modal-select-testcode/modal-select-testcode-multi";
import { ModalSelectOPAlertPage } from "../pages/model-select-opalert/modal-select-opalert";
import { TestCodeSingleComponent} from "../pages/modal-select-testcode/modal-select-testcode-single";
import {ModalSelectSingleWorkPlacePage} from "../pages/modal-select-workplace/modal-select-workplace-single";
import { RuleFollowDifinitionPage } from "../pages/rule-follow-difinition/rule-follow-difinition";
import { ModalRuleFollowAddPage } from '../pages/modal-rule-follow-add/modal-rule-follow-add';

import { ComponentsModule } from "../components/components.module";

import { StatusBar } from "@ionic-native/status-bar";
import { SplashScreen } from "@ionic-native/splash-screen";
import { AuthProvider } from "../providers/auth/auth";
import { HttpClientModule, HttpClient } from "@angular/common/http";
import { UsersPage } from "../pages/users/users";
import { DataProvider } from "../providers/data/data";
import { SampleResultsPage } from "../pages/sample-results/sample-results";
import { SearchProvider } from "../providers/search/search";

import { DemoPage } from "../pages/demo/demo";
@NgModule({
  declarations: [
    MyApp,
    AlertsPage,
    DashboardPage,
    LoginPage,
    SamplesPage,
    SettingsPage,
    SampleResultsPage,
    TabsPage,
    UsersPage,
    RuleWizardPage,
    ModalSelectMrnPage,
    ModalSelectPatientNamePage,
    ModalSelectReqLocationPage,
    ModalSelectWorkPlacePage,
    TestCodeMultiComponent,
    ModalSelectOPAlertPage,
    TestCodeSingleComponent,
    ModalSelectSingleWorkPlacePage,
    SampleExplorerPage,
    ResultValidationPage,
    DemoPage,
    CreateRulePage,
    RuleFollowDifinitionPage,
    ModalRuleFollowAddPage
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    ComponentsModule,
    HttpClientModule,
    IonicStorageModule.forRoot(),

    IonicModule.forRoot(
      MyApp,
      {
        mode: "md",
        tabsPlacement: "top"
      },
      {
        links: [
          { component: TabsPage, name: "tabs", segment: "tabs" },
          { component: DashboardPage, name: "dashboard", segment: "dashboard" },
          { component: AlertsPage, name: "alerts", segment: "alerts" },
          { component: SamplesPage, name: "samples", segment: "samples" },
          { component: SampleResultsPage, name: "results", segment: "results" },
          { component: SettingsPage, name: "settings", segment: "settings" },
          { component: RuleWizardPage, name: "rulewizard", segment: "rulewizard" },
          { component: SampleExplorerPage, name: "sampleexplorer", segment: "sampleexplorerpage" },
          { component: ResultValidationPage, name: "resultvalidation", segment: "resultvalidationpage" },
          { component: CreateRulePage, name: "createrule", segment: "createrulepage" },
          { component: RuleFollowDifinitionPage, name: "ruleflowdefinition", segment: "ruleflowdefinition" },
          { component: ModalRuleFollowAddPage, name: "modalruleflowadd", segment: "modalruleflowadd" },
          { component: DemoPage, name: "demo", segment: "demo" }
        ]
      }
    ),
    IonicStorageModule.forRoot(),
    // MatButtonToggle,
    MatAutocompleteModule,
    MatButtonModule,
    // MatButtonToggleModule,
    MatCardModule,
    MatCheckboxModule,
    MatChipsModule,
    MatDatepickerModule,
    MatDialogModule,
    MatDividerModule,
    MatExpansionModule,
    MatGridListModule,
    MatIconModule,
    MatInputModule,
    MatListModule,
    MatMenuModule,
    MatNativeDateModule,
    MatPaginatorModule,
    MatProgressBarModule,
    MatProgressSpinnerModule,
    MatRadioModule,
    MatRippleModule,
    MatSelectModule,
    MatSidenavModule,
    MatSliderModule,
    MatSlideToggleModule,
    MatSnackBarModule,
    MatSortModule,
    MatStepperModule,
    MatTableModule,
    MatTabsModule,
    MatToolbarModule,
    MatTooltipModule,
    MatFormFieldModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    AlertsPage,
    DashboardPage,
    LoginPage,
    SamplesPage,
    SampleResultsPage,
    SettingsPage,
    TabsPage,
    UsersPage,
    RuleWizardPage,
    ModalSelectMrnPage,
    ModalSelectPatientNamePage,
    ModalSelectReqLocationPage,
    ModalSelectWorkPlacePage,
    TestCodeMultiComponent,
    ModalSelectOPAlertPage,
    TestCodeSingleComponent,
    ModalSelectSingleWorkPlacePage,
    SampleExplorerPage,
    ResultValidationPage,
    DemoPage,
    CreateRulePage,
    RuleFollowDifinitionPage,
    ModalRuleFollowAddPage
  ],
  providers: [
    HttpClient,
    StatusBar,
    SplashScreen,
    MatNativeDateModule,
    { provide: ErrorHandler, useClass: IonicErrorHandler },
    AuthProvider,
    DataProvider,
    SearchProvider
  ]
})
export class AppModule { }
