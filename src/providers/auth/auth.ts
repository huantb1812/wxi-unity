import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { Observable } from "rxjs/Observable";
// import { Storage } from '@ionic/storage';
// import { DataProvider } from "../data/data";
import 'rxjs/add/observable/of';
import "rxjs/add/operator/map";
import "rxjs/add/operator/catch";
import "rxjs/add/operator/mergeMap";
import "rxjs/add/observable/throw";
import { Events } from 'ionic-angular';

import * as _ from 'lodash';

/*
  Generated class for the AuthProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class AuthProvider {

  _user: any;
  constructor(private http: HttpClient, private events: Events) {
    console.log('Hello AuthProvider Provider');
  }

  //TODO: IMPLEMENT FULL API-BASED LOGIN FUNCTIONALITY
  login(params: { username: string, password: string }, user: any = null): Observable<any> {
    //=1
    //sysmex=2
    this._user = {
      FirstName: params.username,
      LastName: params.username,
      FullName: params.username,
      role: -1,
      picture: {
        thumbnail: ""
      }
    };
    if (params.username == "customer" || params.username == "sysmex") {
      if (params.username == "customer") {
        this._user.role = 0
      } else if (params.username == "sysmex") {
        this._user.role = 1
      }
      this.events.publish('USER', this._user);

      return Observable.of(this._user);

    }

    if (user) {
      this._user = user;

      return Observable.of(user);
    }

    if (params.username === null || params.password === null) {
      return Observable.throw("Please insert credentials");
    }
    console.info('login', params.username, params.password);
    return this.http.get('https://randomuser.me/api/?nat=us,dk,fr,gb&inc=name,email,location,login,registered,picture').map((result: any) => {

      this._user = result.results[0];
      this._user.FirstName = _.capitalize(this._user.name.first);
      this._user.LastName = _.capitalize(this._user.name.last);
      this._user.FullName = _.capitalize(this._user.name.first) + ' ' + _.capitalize(this._user.name.last);

      console.info('randomuser', this._user);
      this.events.publish('USER', this._user);

      return this._user;
    });
  }

  users(count: number = 20): Observable<any> {
    return this.http.get('https://randomuser.me/api/?noinfo&results=' + count + '&nat=us,dk,fr,gb&inc=name,email,location,login,registered,picture').map((result: any) => {
      result = _.map(result.results, (u) => {
        u.FirstName = _.capitalize(u.name.first);
        u.LastName = _.capitalize(u.name.last);
        u.FullName = _.capitalize(u.name.first) + ' ' + _.capitalize(u.name.last);
        return u;
      });
      console.info('users', result);
      return result;
    });
  }

  //TODO: VERIFY WHETHER TO IMPLEMENT CACHING OF USER SETTINGS?
  user(): Observable<any> {
    return new Observable((observer) => {
      observer.next(this._user);
      observer.complete();
    });
  }


}
