import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { Observable } from "rxjs/Observable";

import 'rxjs/add/observable/of';
import "rxjs/add/operator/map";
import "rxjs/add/operator/catch";
import "rxjs/add/operator/mergeMap";
import "rxjs/add/observable/throw";
import { ThrowStmt } from '@angular/compiler';

import * as moment from 'moment';
import { SearchModel } from '../model/searchModel';
import { TypeSort } from '../model/typeSort';
/*
 Generated class for the DataProvider provider.

 See https://angular.io/guide/dependency-injection for more info on providers
 and Angular DI.
*/
@Injectable()
export class DataProvider {
  public currentUser: any
  private apiUrl = 'https://dev-wamcloud-265818522.us-east-1.elb.amazonaws.com/services/wamcloudrulesmicroservices/';//rule wizard
  private apiMrnAndName = '';
  private apiWorkPlace = '';
  private apiLocation = '';
  private apiTestCode = '';
  private apiSearchSampleExplorer = 'https://dev-wamcloud-265818522.us-east-1.elb.amazonaws.com/services/wamcloudsamplemicroservices/';//rule wizard
  private apiCreateRule = '';
  private apiOpAlert = '';
  private apiRuleFollow = '';
  private apiRuleByRuleFollow = '';
  private apiUpdateRuleFollow = '';

  constructor(private http: HttpClient) {
    let api = 'https://dev-wamcloud-265818522.us-east-1.elb.amazonaws.com/services/wamcloudmasterdataservices/';//masterdata
    this.apiMrnAndName = api + 'api/patient/';
    this.apiWorkPlace = api + 'api/workplace/';
    this.apiLocation = api + 'api/location/';
    this.apiTestCode = api + 'api/testcode/';
    this.apiOpAlert = api + 'api/opalert/';
    this.apiCreateRule = this.apiUrl + 'api/rules/';
    this.apiRuleFollow = this.apiUrl + 'api/rule_flow/all';
    this.apiRuleByRuleFollow = this.apiUrl + 'api/rule_flow/spec/all?search=ruleFlowId:';
    this.apiUpdateRuleFollow = this.apiUrl + 'api/rule_flow/';
    this.currentUser = null;
    console.log('Hello DataProvider Provider');
  }

  setCurrentUser(user) {
    this.currentUser = user;
  }
  getCurrentUser() {
    return this.currentUser;
  }
  searchRuleFlow(data, page = 1, fieldSort = '', typeSort = TypeSort.none): Observable<any[]> {
    let url = this.apiUrl + `api/rules/rule_flow/search?search=`;//'ruleName:${data.ruleName},discipline:${data.discipline},activeMode:${data.activeMode},triggerOn:${data.triggerOn}&page=0&size=5`;
    //step1
    if (data.ruleName != null) {
      url += `ruleName:${data.ruleName},`;
    }
    if (data.discipline != null) {
      url += `discipline:${data.discipline},`;
    }
    if (data.activeMode != null) {
      url += `activeMode:${data.activeMode},`;
    }
    if (data.triggerOn != null) {
      url += `triggerOn:${data.triggerOn},`;
    }
    //step2:
    if (data.patientType != null) {
      url += `patientType:${data.patientType},`;
    }
    if (data.gender != null) {
      url += `gender:${data.gender},`;
    }
    if (data.mrn != null) {
      var split = data.mrn.split('; ');
      var strSplit= split.join(';');
      url += `mrn:${strSplit},`;
    }
    if (data.patientName != null) {
      var split = data.patientName.split('; ');
      var strSplit= split.join(';');
      url += `patientName:${strSplit},`;
    }
    if (data.reqLocation != null) {
      var split = data.reqLocation.split('; ');
      var strSplit= split.join(';');
      url += `reqLocation:${strSplit},`;
    }
    //step3:
    if (data.testCode.length > 0) {
      let temp = [];
      for (let i = 0; i < data.testCode.length; i++) {
        temp.push(data.testCode[i].testCode);
      }
      url += `testCode:${temp.join(';')},`;
    }

    if (url.length > 0) {
      url = url.substring(0, url.length - 1);
    }

    if (typeSort != TypeSort.none) {
      url += `&sort=${fieldSort},${typeSort}&page=${page - 1}&size=10`;
    }else{
      url += `&sort=ruleId&page=${page - 1}&size=10`;
    }
    return this.http.get<any>(url);
  }
  
  search(data, page = 1, fieldSort = '', typeSort = TypeSort.none): Observable<any[]> {
    let url = this.apiUrl + `api/rules/search?search=`;//'ruleName:${data.ruleName},discipline:${data.discipline},activeMode:${data.activeMode},triggerOn:${data.triggerOn}&page=0&size=5`;
    //step1
    if (data.ruleName != null) {
      url += `ruleName:${data.ruleName},`;
    }
    if (data.discipline != null) {
      url += `discipline:${data.discipline},`;
    }
    if (data.activeMode != null) {
      url += `activeMode:${data.activeMode},`;
    }
    if (data.triggerOn != null) {
      url += `triggerOn:${data.triggerOn},`;
    }
    //step2:
    if (data.patientType != null) {
      url += `patientType:${data.patientType},`;
    }
    if (data.gender != null) {
      url += `gender:${data.gender},`;
    }
    if (data.mrn != null) {
      var split = data.mrn.split('; ');
      var strSplit= split.join(';');
      url += `mrn:${strSplit},`;
    }
    if (data.patientName != null) {
      var split = data.patientName.split('; ');
      var strSplit= split.join(';');
      url += `patientName:${strSplit},`;
    }
    if (data.reqLocation != null) {
      var split = data.reqLocation.split('; ');
      var strSplit= split.join(';');
      url += `reqLocation:${strSplit},`;
    }
    //step3:
    if (data.testCode.length > 0) {
      let temp = [];
      for (let i = 0; i < data.testCode.length; i++) {
        temp.push(data.testCode[i].testCode);
      }
      url += `testCode:${temp.join(';')},`;
    }

    if (url.length > 0) {
      url = url.substring(0, url.length - 1);
    }
    if (typeSort != TypeSort.none) {
      url += `&sort=${fieldSort},${typeSort}&page=${page - 1}&size=10`;
    }else{
      url += `&sort=ruleId&page=${page - 1}&size=10`;
    }    

    return this.http.get<any>(url);
  }
  getTestCode(page = 1): Observable<any[]> {
    let url = this.apiTestCode + `?page=${page - 1}&size=10`;
    return this.http.get<any>(url);
  }
  searchSampleExplorer(model, page = 1, fieldSort = '', typeSort = TypeSort.none): Observable<any> {
    var apiSearchSample = this.apiSearchSampleExplorer + 'api/order_demographic_data/search?';
    if (model.SampleId) {
      apiSearchSample += `sampleId=${model.SampleId}`;

    }
    if (model.receiptDateFrom) {
      apiSearchSample += `&receiptDateFrom=${model.receiptDateFrom}`;
    }
    if (model.receiptDateTo) {
      apiSearchSample += `&receiptDateTo=${model.receiptDateTo}`;
    }
    //workplace
    let temp = [];
    if (model.workplace) {
      temp = [];
      temp = model.workplace.split(';') || [];
      if (temp.length > 0) {
        apiSearchSample += `&workplace=${temp.join('&workplace=')}`;
      }
    }
    //mrn
    if (model.mrn) {
      temp = [];
      temp = model.mrn.split(';') || [];
      if (temp.length > 0) {
        apiSearchSample += `&mrn=${temp.join('&mrn=')}`;
      }
    }
    //name
    if (model.patientName) {
      // apiSearchSample += `patientName=${model.patientName},`;
      temp = [];
      temp = model.patientName.split(';') || [];
      if (temp.length > 0) {
        apiSearchSample += `&patientName=${temp.join('&patientName=')}`;
      }
    }

    //location
    if (model.requestingLocation) {
      // apiSearchSample += `requestingLocation=${model.requestingLocation},`;
      temp = [];
      temp = model.requestingLocation.split(';') || [];
      if (temp.length > 0) {
        apiSearchSample += `&requestingLocation=${temp.join('&requestingLocation=')}`;
      }
    }
    apiSearchSample += `&page=${page - 1}&size=1000`;
    // debugger;
    if (typeSort != TypeSort.none) {
      apiSearchSample += `&sort=${fieldSort},${typeSort}`
    }
    return this.http.get<any>(apiSearchSample);
  }
  // 
  get(path: string): Observable<any> {
    return this.http.get(path);
  }

  results(): Observable<any> {
    return this.get('assets/data/results-mock.json').map(result => {
      return result.map(item => {
        item.sample_date = moment(item.sample_date).toDate();
        return item;
      })
    });
  }

  //Placeholder for API to return tally counts (stat orders or all orders)
  tallyCounter(isStat: boolean = true) {
    if (isStat) {
      return [
        {
          label: 'Pending Validation',
          tally: 2
        },
        {
          label: 'Pending Reruns',
          tally: 6
        },
        {
          label: 'Pending Orders',
          tally: 8
        },
        {
          label: 'Alerts',
          tally: 0
        }
      ]
    }

    return [
      {
        label: 'Pending Validation',
        tally: 8
      },
      {
        label: 'Pending Reruns',
        tally: 15
      },
      {
        label: 'Pending Orders',
        tally: 28
      },
      {
        label: 'Alerts',
        tally: 1
      }
    ]
  }
  getMrnAndName(page = 1): Observable<any[]> {
    let url = this.apiMrnAndName + `?page=${page - 1}&size=10`;
    return this.http.get<any>(url);
  }

  getWorkPlace(page = 1): Observable<any[]> {
    let url = this.apiWorkPlace + `?page=${page - 1}&size=10`;
    return this.http.get<any>(url);
  }

  getLocation(page = 1): Observable<any[]> {
    let url = this.apiLocation + `?page=${page - 1}&size=10`;
    return this.http.get<any>(url);
  }
  getOpAlert(page = 1): Observable<any[]> {
    let url = this.apiOpAlert + `?page=${page - 1}&size=10`;
    return this.http.get<any>(url);
  }
  apps() {
    return [
      {
        url: 'rule-wizard',
        page: 'rulewizard',
        title: 'Rule Wizard',
        desc: null,
        icon: 'i8-search-property',
        status: 1
      },     
      {
        url: 'ruleflowdefinition',
        page: 'ruleflowdefinition',
        title: 'Rule Flow Definition',
        desc: null,
        icon: 'i8-clipboard',
        status: 1
      },
      //  {
      //   url: 'sample-explorer',
      //   page: 'sampleexplorer',
      //   title: 'sample-explorer',
      //   desc: null,
      //   icon: 'i8-approval',
      //   status: 1
      // },
      //  {
      //   url: 'result-validation',
      //   page: 'resultvalidation',
      //   title: 'result-validation',
      //   desc: null,
      //   icon: 'i8-approval',
      //   status: 1
      // }, 
      {
        url: 'http://bcqmdev.sysmexamerica.com/',
        title: 'Auto-Validation',
        desc: null,
        icon: 'i8-approval'
      }, {
        url: 'http://www.sysmexeducation.com/dashboard.asp',
        title: 'Barcode Errors',
        desc: null,
        icon: 'i8-barcode-scanner'
      }, {
        url: 'https://dashboard.sysmex.com',
        title: 'Critical Results',
        desc: null,
        icon: 'i8-high-priority'
      }, 
      // {
      //   url: 'https://www.sysmex.com/CRC/Pages/Quality-Control-Information-and-Insight.aspx',
      //   title: 'Management Reports',
      //   desc: null,
      //   icon: 'i8-clipboard'
      // }, 
      {
        url: 'https://www.sysmex.com/CRC/Pages/Quality-Control-Information-and-Insight.aspx',
        title: 'Manual Order Entry',
        desc: null,
        icon: 'smx-glyphish-handtruck'
      }
      // }, {
      //   url: 'https://www.sysmex.com/crc',
      //   title: 'Modify Sample ID',
      //   desc: null,
      //   icon: 'i8-search-property'
      // }
      , {
        url: 'http://sysmexconnect.com/wam-2014',
        title: 'Smear Status',
        desc: null,
        icon: 'i8-microscope'
      }, {
        url: 'http://sysmexconnect.com/wam-2014',
        title: 'Specimen Tracking',
        desc: null,
        icon: 'smx-glyphish-location-pin'
      }, {
        url: 'http://sysmexconnect.com/wam-2014',
        title: 'Unregistered Results',
        desc: null,
        icon: 'i8-case'
      }
    ];
  }

  getDataSearch(searchModel: SearchModel) {
    let data =
      {
        activeMode: null,
        ageFrom: null,
        ageTo: null,
        discipline: null,
        gender: null,
        links: [],
        mrn: null,
        priority: null,
        // patientType: null,
        patientType: null,
        patientName: null,
        // priority: searchModel.priority,
        reqLocation: null,
        ruleDesc: null,
        ruleName: null,
        triggerOn: null,
        testCode: []
      }
    if (searchModel.activeMode > -1) {
      data.activeMode = searchModel.activeMode == 1 ? "T" : "F";
    }
    if (searchModel.discipline > -1) {
      data.discipline = searchModel.discipline == 1 ? "HEMA" : "CHEM";
    }
    if (searchModel.gender > -1) {
      data.gender = searchModel.gender; //== 1 ? "M" : "F";
    }
    if (searchModel.mrn.length > 0) {
      data.mrn = searchModel.mrn;
    }
    if (searchModel.patientName.length > 0) {
      data.patientName = searchModel.patientName;
    }
    if (searchModel.patientType > -1) {
      data.patientType = searchModel.patientType;
    }
    if (searchModel.reqLocation.length > 0) {
      data.reqLocation = searchModel.reqLocation;
    }
    if (searchModel.ruleName.length > 0) {
      data.ruleName = searchModel.ruleName
    }
    if (searchModel.triggerOn > -1) {
      data.triggerOn = searchModel.triggerOn;
    }

    data.testCode = searchModel.testCode || [];
    return data;
  }

  getPager(totalItems: number, currentPage: number = 1, pageSize: number = 10) {
    // calculate total pages

    let totalPages = Math.ceil(totalItems / pageSize);

    let startPage: number, endPage: number;
    if (totalPages <= 10) {
      // less than 10 total pages so show all
      startPage = 1;
      endPage = totalPages;
    } else {
      // more than 10 total pages so calculate start and end pages
      if (currentPage < 6) {
        startPage = 1;
        endPage = 10;
      } else if (currentPage + 4 >= totalPages) {
        startPage = totalPages - 9;
        endPage = totalPages;
      } else {
        startPage = currentPage - 5;
        endPage = currentPage + 4;
      }
    }

    // calculate start and end item indexes
    let startIndex = (currentPage - 1) * pageSize;
    let endIndex = Math.min(startIndex + pageSize - 1, totalItems - 1);

    // create an array of pages to ng-repeat in the pager control
    let pages = [];
    for (var i = startPage; i < endPage + 1; i++) {
      pages.push(i);
    }
    //  let pages= _.range(startPage, endPage + 1);



    // return object with all pager properties required by the view
    return {
      totalItems: totalItems,
      currentPage: currentPage,
      pageSize: pageSize,
      totalPages: totalPages,
      startPage: startPage,
      endPage: endPage,
      startIndex: startIndex,
      endIndex: endIndex,
      pages: pages
    };
  }

  saverule(RuleModel): Observable<any> {
    return this.http.post<any>(this.apiCreateRule, RuleModel);
  }
  updaterule(RuleModel): Observable<any> {
    return this.http.put<any>(this.apiCreateRule, RuleModel);
  }

  loadOrderResults(period: string, lid: string, orderNumber: string, typeSort = TypeSort.asc): Observable<any> {
    let url = this.apiSearchSampleExplorer + `api/order_result/spec?search=lPeriod:${period},lId:${lid},orderNumber:${orderNumber}&page=0&size=100`;
    if(typeSort == TypeSort.desc){
      url +=`&sort=testCode,DESC`;
    }else{
      typeSort = TypeSort.asc;
      url +=`&sort=testCode,ASC`;
    }
    return this.http.get<any>(url);
  }

  loadOpAlert(period: string, lid: string, orderNumber: string): Observable<any> {
    let url = this.apiSearchSampleExplorer + `api/executed_op_alert/spec?search=lPeriod:${period},lId:${lid},orderNumber:${orderNumber}&page=0&size=100&sort=id,ASC`;
    return this.http.get<any>(url);
  }
  //rule follow difinition
  getRuleFollows() {
    return this.http.get<any>(this.apiRuleFollow);
  }

  updateRuleFollowDifinition(ruleFollow) {
    return this.http.put<any>(this.apiUpdateRuleFollow, ruleFollow);
  }
  array_move(arr, old_index, new_index) {
    if (new_index >= arr.length) {
      var k = new_index - arr.length + 1;
      while (k--) {
        arr.push(undefined);
      }
    }
    arr.splice(new_index, 0, arr.splice(old_index, 1)[0]);
    return arr;
  };

}
