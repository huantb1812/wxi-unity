export class SearchModel {
    //step1:
    public ruleName: string;
    public discipline: number;
    public activeMode: number;
    public triggerOn: number;
 
    //step2:
    public patientType: number;
    
    public gender: number;
    public mrn: string;
    public mrnSelectedArray: Array<any>;
    public patientName: string;
    public patientNameSelectedArray: Array<any>;
    public reqLocation: string;
    public locationSelectedArray: Array<any>;

    //step3:
    public testCode: Array<any>;

    public ageFrom: number;
    public ageTo: number;
    public links: Array<any>;
    public priority: number;
    public ruleDesc: string;


    constructor() {
        this.ruleName = "";
        this.discipline = -1;
        this.activeMode = -1;
        this.triggerOn = -1;
        this.patientType = -1;

        this.gender = -1;
        this.mrn = "";
        this.mrnSelectedArray=[];
        this.patientName = "";
        this.patientNameSelectedArray=[];
        this.reqLocation = "";
        this.locationSelectedArray=[];

        this.testCode = [];
        
    }
}