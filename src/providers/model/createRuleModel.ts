import * as moment from "moment";
import * as _ from 'lodash';
export class CreateRuleModel {

    public ruleId: number;
    public ruleName: string;
    public priority: string;
    public description: string;
    public active: boolean;
    public activeDateFrom: string;
    public activeDateTo: string;
    public triggerOn: number;
    public runSelection: number;
    public patientAgeFrom: string;
    public patientAgeTo: string;
    public patientAgeFromUnit: string;
    public patientAgeToUnit: string;
    public specimenAgeFrom: string;
    public specimenAgeTo: string;
    public specimenAgeFromUnit: string;
    public specimenAgeToUnit: string;
    public gender: number;
    public patientType: number;
    public discipline: number;
    public physicianNumber: string;
    public reqPhys: string;
    public reqLocation: string;
    public careUnit: string;
    public site: string;
    public mrn: string;
    public name: string;
    public diagnosis1: string;
    public diagnosis2: string;
    public stat: boolean;
    //tab2 
    public ruleElements: Array<any>;
    //check valid create/ edit
    public checkValid: any;

    //action
    public opAlert: string;
    public reRun: boolean;
    public rerunSelection: number;
    public rerunTestList: string;
    public review: boolean;
    public reviewSelection: number;
    public reviewTestList: string;
    public forceStatus: string;
    public reflexTestList: string;
    public deleteTestList: string;
    public oPAlertId: string;
    public oPAlertDescription: string;
    public oPAlertPriority: string;

    public reRunTestListArray: Array<any>;
    public reviewTestListArray: Array<any>;
    public reflexTestListArray: Array<any>;
    public deleteTestListArray: Array<any>;
    public opPriorityArray: Array<any>;

    constructor() {
        let currentDate = moment().format('YYYY-MM-DD');

        this.ruleName = "";
        this.priority = "";
        this.description = "";
        this.active = false;
        this.activeDateFrom = currentDate;
        this.activeDateTo = currentDate;
        this.triggerOn = 1;
        this.runSelection = 1;
        this.patientAgeFrom = "";
        this.patientAgeTo = "";
        this.patientAgeFromUnit = 'Y';
        this.patientAgeToUnit = 'Y';
        this.specimenAgeFrom = "";
        this.specimenAgeTo = "";
        this.specimenAgeFromUnit = 'D';
        this.specimenAgeToUnit = 'D';
        this.gender = 1;
        this.patientType = 1;
        this.discipline = 1;
        this.physicianNumber = "";
        this.reqPhys = "";
        this.reqLocation = "";
        this.careUnit = "";
        this.site = "";
        this.mrn = "";
        this.name = "";
        this.diagnosis1 = "";
        this.diagnosis2 = "";
        this.stat = true;
        //tab2
        this.ruleElements = [];
        this.checkValid = {};
        //action
        this.opAlert = "";
        this.reRun = false;
        this.rerunSelection = 0;
        this.rerunTestList = "";
        this.review = false;
        this.reviewSelection = 0;
        this.reviewTestList = "";
        this.forceStatus = "";
        this.reflexTestList = "";
        this.deleteTestList = "";
        this.oPAlertId = "";
        this.oPAlertDescription = "";
        this.oPAlertPriority = "";
        this.reRunTestListArray = new Array<any>();
        this.reviewTestListArray = new Array<any>();
        this.reflexTestListArray = new Array<any>();
        this.deleteTestListArray = new Array<any>();

    }
    public valid() {
        let t = [];
        if (this.ruleName == '') {
            t.push({ fieldName: 'ruleName', message: 'Rule Name field is required.' })
        }


        if (this.site && this.site.length != 0) {
            let regexp = new RegExp('([A-Z][|,&])*[A-Z]$');
            let isMatch = regexp.test(this.site);
            if (isMatch == false) {
                t.push({ fieldName: 'site', message: 'Site - the format is not corect (Require And/or and wild card statement, example: A|B|C|D).' })
            }
        }
        if (this.careUnit && this.careUnit.length != 0) {
            let regexp = new RegExp('([A-Z][|,&])*[A-Z]$');
            let isMatch = regexp.test(this.site);
            if (isMatch == false) {
                t.push({ fieldName: 'careUnit', message: 'Care Unit - the format is not corect (Require And/or and wild card statement, example: A|B|C|D).' })
            }
        }
        var checkTestCode = false;
        if (this.ruleElements.length == 0) {
            t.push({ field: 'testCode', message: 'At least one Test should be added.' });
        } else {
            var arrTestCode = _.filter(this.ruleElements, function (o) { return !o.testCode });
            if (arrTestCode.length == this.ruleElements.length) {
                t.push({ field: 'testCode', message: 'At least one Test should be added.' });
                checkTestCode = true;
            }else if(arrTestCode.length > 0){
                t.push({ field: 'testCode', message: 'Test is required' });
                checkTestCode = true;
            }
            if(!checkTestCode){
                for(var i = 0; i< this.ruleElements.length; i++){
                    var item = this.ruleElements[i];
                    if(item.checkType == 1 || item.checkType == 2){
                        if(!item.lowLimit || !item.highLimit){
                            t.push({field:'lowHigh',message:'The Low/High end of the result range is required.', position: i});                            
                            break;
                        }      
                    }
                }
	        }

            
        }
        if (this.reRun && this.rerunSelection == 3) {
            if (this.rerunTestList == null || this.rerunTestList == "") {
                t.push({ field: 'rerunTestList', message: 'At least one Test code should be added.' });
            }
        }
        if (this.review && this.reviewSelection == 3) {
            if (this.reviewTestList == null || this.reviewTestList == "") {
                t.push({ field: 'reviewTestList', message: 'At least one Test code should be added.' });
            }
        }
        return t;
    }
    public wrapInput(input) {
        if (input != undefined) {

            this.ruleId = input.ruleId;
            this.ruleName = input.ruleName;
            this.description = input.ruleDesc;
            this.priority = input.priority;
            this.active = input.activeMode == 'T' ? true : false;
            this.activeDateFrom = input.activeDateFrom == null ? null : moment(input.activeDateFrom).format('YYYY-MM-DD');
            this.activeDateTo = input.activeDateTo == null ? null : moment(input.activeDateTo).format('YYYY-MM-DD');
            this.triggerOn = input.triggerOn;
            this.runSelection = input.runSelection;

            this.patientAgeFrom = input.ageFrom;
            this.patientAgeTo = input.ageTo;
            this.patientAgeFromUnit = input.ageFromUnit;
            this.patientAgeToUnit = input.ageToUnit;

            this.specimenAgeFrom = input.specimenAgeFrom;
            this.specimenAgeTo = input.specimenAgeTo;
            this.specimenAgeFromUnit=input.specimenAgeFromUnit;
            this.specimenAgeToUnit=input.specimenAgeToUnit;

            this.gender = input.gender || null;
            this.patientType = input.patientType || null;
            this.discipline = input.discipline;
            this.physicianNumber = input.reqPhysicianNumber;
            this.reqPhys = input.reqPhysician;
            this.reqLocation = input.reqLocation;
            this.careUnit = input.careUnit;
            this.site = input.site;
            this.mrn = input.mrn;
            this.name = input.patientName;
            this.diagnosis1 = input.diagnosisCode1;
            this.diagnosis2 = input.diagnosisCode2;
            this.stat = input.statFlag=='T'?true:false;
            //tab2
            this.ruleElements = input.ruleElements;
            console.log(this.ruleElements);
            //action
            this.reRun = input.rerunFlag == 'T' ? true : false;
            this.review = input.reviewFlag == 'T' ? true : false;
            this.oPAlertPriority = input.opAlertType;
            this.oPAlertId = input.opAlert;
            this.oPAlertDescription = input.opAlertText;
            this.rerunSelection = input.rerunSelection;
            this.reviewSelection = input.reviewSelection;
            this.rerunTestList = input.rerunTestList;
            this.reviewTestList = input.reviewTestList;
            this.reflexTestList = input.reflexTestList;
            this.deleteTestList = input.deleteTestList;
            this.forceStatus = input.reviewResultStatus;
            
            if (input.rerunTestList) {
                var tempArray = input.rerunTestList.split(",");
                var self = this;
                tempArray.forEach(function (element) {
                    var arr = { testCode: element };
                    self.reRunTestListArray.push(arr);
                });
            }
            if (input.reviewTestList) {
                var tempArrayreviewTestList = input.reviewTestList.split(",");
                var self = this;
                tempArrayreviewTestList.forEach(function (element) {
                    var arr = { testCode: element };
                    self.reviewTestListArray.push(arr);
                });
            }
            if (input.reflexTestList) {
                var tempArrayReflexTestList = input.reflexTestList.split(",");
                var self = this;
                tempArrayReflexTestList.forEach(function (element) {
                    var arr = { testCode: element };
                    self.reflexTestListArray.push(arr);
                });
            }
            if (input.deleteTestList) {
                var tempArrayDeleteTestList = input.deleteTestList.split(",");
                var self = this;
                tempArrayDeleteTestList.forEach(function (element) {
                    var arr = { testCode: element };
                    self.deleteTestListArray.push(arr);
                });
            }

        }
    }
    public wrapOutput() {
        let t = {
            ruleId: null,
            ruleName: this.ruleName,
            ruleDesc: this.description,
            priority: this.priority,
            activeMode: this.active == true ? 'T' : 'F',
            activeDateFrom: this.activeDateFrom,
            activeDateTo: this.activeDateTo,
            triggerOn: this.triggerOn,
            runSelection: this.runSelection,

            ageFrom: this.patientAgeFrom,
            ageTo: this.patientAgeTo,
            ageFromUnit:this.patientAgeFromUnit,
            ageToUnit:this.patientAgeToUnit,

            specimenAgeFrom: this.specimenAgeFrom,
            specimenAgeTo: this.specimenAgeTo,
            specimenAgeFromUnit:this.specimenAgeFromUnit,
            specimenAgeToUnit:this.specimenAgeToUnit,


            gender: this.gender ? this.gender : null,
            patientType: this.patientType,
            discipline: this.discipline,
            reqPhysicianNumber: this.physicianNumber,
            reqPhysician: this.reqPhys,
            reqLocation: this.reqLocation,
            careUnit: this.careUnit,
            site: this.site,
            mrn: this.mrn,
            patientName: this.name,
            diagnosisCode1: this.diagnosis1,
            diagnosisCode2: this.diagnosis2,
            statFlag: this.stat == true ? 'T' : 'F',
            //tab2
            ruleElements: this.ruleElements,

            //action
            rerunTestList: this.rerunTestList,
            rerunFlag: this.reRun == true ? 'T' : 'F',
            reviewFlag: this.review == true ? 'T' : 'F',
            reviewTestList: this.reviewTestList,
            reflexTestList: this.reflexTestList,
            deleteTestList: this.deleteTestList,
            opAlertType: this.oPAlertPriority,
            opAlert: this.oPAlertId,
            opAlertText: this.oPAlertDescription,
            rerunSelection: this.rerunSelection+'',
            reviewSelection: this.reviewSelection,
            reviewResultStatus: this.forceStatus
            /*
                        this.oPAlertPriority = input.opAlertType;
            this.oPAlertId = input.opAlert;
            this.oPAlertDescription = input.opAlertText;
             */
        };
        return t;
    }



}