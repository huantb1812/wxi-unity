export class SampleExplorerModel {
    // public fromSampleId: string;
    // public toSampleId: string;
    public sampleId: string;
    public receiptDateFrom: Date;
    public receiptDateTo: Date;
    public workplace: string;
    public mrn: string;
    public patientName: string;
    public requestingLocation: string;
    constructor() {
        // this.fromSampleId = "";
        // this.toSampleId = "";
        this.sampleId = "";
        this.workplace = "";
        this.mrn = "";
        this.patientName = "";
        this.requestingLocation = ""
    }
}

/*
{
	"pk": {
		"lId": "LID2",
		"lPeriod": "LPERIOD2",
		"orderNumber": "ORDER2"
	},
	"sampleId": "ID2",
	"discipline": null,
	"requestingLocation": null,
	"requestingPhysician": null,
	"physicianName": null,
	"physicianPhone": null,
	"careUnit": null,
	"roomOrBed": null,
	"mrn": null,
	"patientName": null,
	"patientBirthDate": null,
	"patientSex": null,
	"patientType": null,
	"receiptDateTime": null,
	"sampleDateTime": null,
	"stat": null,
	"site": null,
	"originType": null,
	"resultStatus": null,
	"diagnosis1": null,
	"diagnosis2": null,
	"statMode": null,
	"critical": null,
	"age": null,
	"reportDate": null,
	"dispatchCodes": null,
	"dateModified": null,
	"modifyingUser": null,
	"dateCreated": null,
	"creatingUser": null,
	"workplace": null,
	"refGroup": null,
	"extMm": null,
	"pregnancyFlag": null,
	"externalReference": null,
	"fastingSt": null,
	"trtNb": null,
	"executedOPAlert": null,
	"sampleTrackingData": null
}
 */