var config = require('@ionic/app-scripts/config/sass.config');
if(config.includePaths && config.includePaths.length) {
  config.includePaths.push('node_modules/flexboxgrid-sass/dist/flexboxgrid');
  config.includePaths.push('node_modules');
  //config.includePaths.push('node_modules/@angular/material/prebuilt-themes/indigo-pink');
  console.log('-- pushing');
} else {
  config.includePaths = [
    'node_modules/ionic-angular/themes',
    'node_modules/ionicons/dist/scss',
    'node_modules/ionic-angular/fonts',
    'node_modules/flexboxgrid-sass/dist/flexboxgrid'
  ];
}

console.log('Added custom sass paths');
module.exports = config;
